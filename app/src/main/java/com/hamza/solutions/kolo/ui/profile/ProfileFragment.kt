package com.hamza.rawaaprovider.ui.profile

import android.app.Activity
import android.content.res.Configuration
import android.view.View
import br.com.joinersa.oooalertdialog.Animation
import br.com.joinersa.oooalertdialog.OnClickListener
import br.com.joinersa.oooalertdialog.OoOAlertDialog
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.rawaa.ui.profile.ProfileContract
import com.hamza.rawaa.ui.profile.ProfilePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Profile
import com.hamza.solutions.kolo.ui.login.SignInActivity
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_profiile.*
import java.util.*

class ProfileFragment : BaseFragment(), ProfileContract.View {
    private lateinit var profilePresenter: ProfileContract.Presenter<ProfileFragment>

    private lateinit var loginPref: LoginPref

    override fun getFragmentView(): Int = R.layout.fragment_profiile

    override fun setUp(view: View?) {

        loginPref = baseActivity?.let { LoginPref(it) }!!
        val user = loginPref?.getUser()
        profilePresenter = ProfilePresenter()
        profilePresenter.onAttach(this)
        logo.loadImage(R.drawable.kolologo)
        profilePresenter.getProfile()


        arabicIV.setOnClickListener {
            showLoading()
            val locale = Locale("ar")
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            config.fontScale = 0.90.toFloat()
            context?.let { LoginPref(it).saveLang("ar") }
            baseActivity!!.getResources().updateConfiguration(config, baseActivity!!.getResources().getDisplayMetrics())
            baseActivity!!.recreate()

        }

        engIV.setOnClickListener {
            showLoading()
            val locale = Locale("en")
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            config.fontScale = 0.90.toFloat()
            context?.let { LoginPref(it).saveLang("en") }
            baseActivity!!.getResources().updateConfiguration(config, baseActivity!!.getResources().getDisplayMetrics())
            baseActivity!!.recreate()

        }
    }

    override fun onProfileRecevied(profile: Profile) {

        profilenameTV.text = profile.name
        profileemailTV.text = profile.email
        profilephoneTV.text = profile.phone
        profilepointsTV.text = profile.poients + getString(R.string.points)

        logoB.setOnClickListener {

            OoOAlertDialog.Builder(context as Activity?)
                    .setTitle(getString(R.string.logout_dialog))
                    .setMessage(getString(R.string.are_you_sure_logout))
                    .setAnimation(Animation.POP)
                    .setPositiveButtonColor(R.color.colorAccent)
                    .setNegativeButtonColor(R.color.colorAccent)
                    .setNegativeButtonTextColor(R.color.whiteColor)
                    .setPositiveButtonTextColor(R.color.whiteColor)
                    .setTitleColor(R.color.colorAccent)
                    .setPositiveButton(getString(R.string.yes), OnClickListener {
                        loginPref?.logout()
                        activity?.let { it1 -> it1.startActivity(SignInActivity.getStartIntent(it1)) }
                        baseActivity?.finish()
                    })
                    .setNegativeButton(getString(R.string.no), null)
                    .build()
        }
    }

    override fun onDestroy() {
        profilePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun newInstance(): ProfileFragment = ProfileFragment()
    }
}