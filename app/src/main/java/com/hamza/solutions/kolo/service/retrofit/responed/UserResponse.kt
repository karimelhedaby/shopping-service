package com.rawaa.hamza.rawaaprovider.service.responed

import com.google.gson.annotations.SerializedName
import com.hamza.solutions.kolo.model.UserSignUp


data class UserResponse(
        //
//        @SerializedName(value = "user", alternate = ["userDetails"])
    @SerializedName("user")
    val userSignUp: UserSignUp? = null,

    @SerializedName("token")
    val token: String? = null
)