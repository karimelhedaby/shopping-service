package com.hamza.solutions.kolo.ui.clothes.products.details.adapter.size

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.hamza.solutions.kolo.R
import kotlinx.android.synthetic.main.item_size.view.*

class SizeAdapter(var sizes: MutableList<String>) : RecyclerView.Adapter<SizeVH>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SizeVH {
        return SizeVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_size, null))
    }

    override fun onBindViewHolder(holder: SizeVH, position: Int) {
        with(holder.itemView) {
            sizeTV.text = sizes[position]
        }
    }

    override fun getItemCount(): Int {
        return sizes.size
    }
}


