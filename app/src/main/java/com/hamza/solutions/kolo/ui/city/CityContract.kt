package com.hamza.solutions.kolo.ui.city;

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.City


interface CityContract {

    interface View : MvpView {
        fun onCitiesRecived(cities: List<City>)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getCities()
    }
}