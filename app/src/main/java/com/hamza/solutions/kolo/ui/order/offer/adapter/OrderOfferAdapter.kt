package com.hamza.solutions.kolo.ui.order.offer.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.OrderOffer
import kotlinx.android.synthetic.main.item_cart_order.view.*
import java.util.*

/**
 * Created by karim on 7/18/18.
 */
class OrderOfferAdapter(var context: Context, var orders: List<OrderOffer>, var orderListener: OrderOfferAdapter.orderClick) : RecyclerView.Adapter<OrderOfferAdapter.OfferVH>() {


    class OfferVH(itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferVH {
        return OfferVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_cart_order, null))
    }

    override fun onBindViewHolder(holder: OfferVH, position: Int) {

        with(holder.itemView) {
            orderDiscountTV.text = orders[position].offers.discound
            Glide.with(context)
                    .load(orders[position].offers.imgs[0])
                    .into(productIV)
            priceTV.text = orders[position].total + " L.E"

            val date = DateUtils
                    .getRelativeTimeSpanString(com.rawaa.hamza.rawaa.utils.DateUtils.convertUTCZToMills(orders[position].creationDate),
                            Calendar.getInstance().timeInMillis, DateUtils.MINUTE_IN_MILLIS) as String
            dateTV.text = date

            val statusIcon = when (orders[position].status) {
                "Pindding" -> R.drawable.ic_cart
                "Accepted" -> R.drawable.ic_verify
                "Rejected" -> R.drawable.ic_refuse
                "OnTheWay" -> R.drawable.ic_shopping_delivering
                "Done" -> R.drawable.ic_delivered
                "Cancle" -> R.drawable.ic_cancle
                else -> 0
            }

            statusIV.setImageDrawable((ContextCompat.getDrawable(context, statusIcon)))

            setOnClickListener {
                orderListener.onOrderClickListener(orders[position])
            }
        }
    }

    open interface orderClick {
        fun onOrderClickListener(orderoffers: OrderOffer)
    }

    override fun getItemCount(): Int {
        return orders.size
    }
}