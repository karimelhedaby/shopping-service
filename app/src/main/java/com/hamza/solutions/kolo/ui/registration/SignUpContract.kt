package com.hamza.rawaa.ui.register
import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse

interface SignUpContract {

    interface View : MvpView {
        fun onSuccess(user: UserResponse)
    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun signUp(name: String?, email: String?, phone: String?, password: String?, confirmPassword: String?)
        fun subscribeNotification(token: String)
    }
}