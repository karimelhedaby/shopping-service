package com.hamza.solutions.kolo.ui.clothes.products.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Product
import com.hamza.solutions.kolo.ui.clothes.AddProduct
import com.hamza.solutions.kolo.ui.clothes.RemoveProduct
import kotlinx.android.synthetic.main.item_product.view.*
import org.greenrobot.eventbus.EventBus

class ProductAdapter(var products: List<Product>, var poductListener: ProductClick) : RecyclerView.Adapter<ShopVH>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopVH {
        return ShopVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_product, null))
    }

    override fun onBindViewHolder(holder: ShopVH, position: Int) {
//        holder.bind(items[position])

        with(holder.itemView) {
            if (products[position].imgs.isNotEmpty()) {
                productIV.loadImage(products[position].imgs[0])
            }
//
            nameTV.text = products[position].title
            priceTV.text = "${products[position].price} L.E"


            setOnClickListener {
                poductListener.onProductClickListener(products[position])
            }

            quantityPicker.setClickNumberPickerListener { _, currentValue, _ ->
                products[position].quantity = currentValue.toInt()
                if (products[position].quantity == 0)
                    EventBus.getDefault().post(RemoveProduct(products[position]))
                else
                    EventBus.getDefault().post(AddProduct(products[position]))
            }
        }

    }


    open interface ProductClick {
        fun onProductClickListener(product: Product)
    }

    override fun getItemCount(): Int {
        return return products.size
    }
}


