package com.rawaa.hamza.rawaaprovider.service.retrofit

import com.hamza.solutions.kolo.model.*
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import io.reactivex.Observable
import retrofit2.http.*

interface Service {
    @POST("login")
    fun signIn(@Body userSignUp: UserSignUp): Observable<UserResponse>

    @POST("signup")
    fun signUp(@Body userSignUp: UserSignUp): Observable<UserResponse>

    @GET("category")
    fun getCategory(): Observable<List<Category>>

    @GET("category/{id}/shopping-markets")
    fun getShops(@Path("id") id: String,
                 @Query("city") city: String): Observable<List<Shop>>

    @GET("category/{id}/shopping-markets")
    fun getClothsShopsByType(@Path("id") id: String,
                             @Query("genderTarget") type: String,
                             @Query("city") city : String): Observable<List<Shop>>

    @GET("markets/{id}/products")
    fun getProducts(@Path("id") id: String): Observable<List<Product>>

    @GET("markets/{id}/products")
    fun searchProducts(@Path("id") id: String, @Query("q") search_keyword: String): Observable<List<Product>>

    @GET("offers")
    fun getOffer(): Observable<List<Offer>>

    @GET("profile")
    fun getProfile(): Observable<Profile>

    @POST("order-offers")
    fun orderOffer(@Body order: OrderOfferRequest): Observable<OrderOffer>

    @POST("products-orders")
    fun orderProducts(@Body order: Order): Observable<OrderResponse>

    @GET("users/{id}/products-orders")
    fun getProductOrders(@Path("id") id: String): Observable<List<ProductsOrders>>

    @GET("users/{user}/offer-orders")
    fun getOrderOffers(@Path("user") user: String): Observable<List<OrderOffer>>

    @PUT("products-orders/{id}/done")
    fun sendOrderDone(@Path("id") orderId: String): Observable<OrderDoneResponse>

    @PUT("/done/{id}")
    fun sendOrderOfferDone(@Path("id") offerId: String): Observable<OrderOfferDoneResponse>

    @GET("delivery-price")
    fun getDeliveryPrice(): Observable<Delivery>

    @PUT("cancle/{id}")
    fun cancleOfferOrder(@Path("id") orderId: String): Observable<CancleOfferOrderResponse>

    @PUT("products-orders/{id}/cancle")
    fun cancleOrder(@Path("id") orderId: String): Observable<CancleOrderResponse>

    @GET("city")
    fun getCity(): Observable<List<City>>

    @GET("ads")
    fun getAds(): Observable<ArrayList<AdsResponse>>

}