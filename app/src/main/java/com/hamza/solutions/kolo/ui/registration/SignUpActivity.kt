package com.hamza.solutions.kolo.ui.registration

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.rawaa.ui.register.SignUpContract
import com.hamza.rawaaprovider.ui.register.SignUpPresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.city.CityFragment
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.activity_sign_up.*


class SignUpActivity : BaseActivity(), SignUpContract.View {


    private lateinit var signUpPresenter: SignUpContract.Presenter<SignUpActivity>

    override fun getActivityView(): Int = R.layout.activity_sign_up

    override fun afterInflation(savedInstance: Bundle?) {
        signUpPresenter = SignUpPresenter()
        signUpPresenter.onAttach(this)
        backgroundIV.loadImage(R.drawable.splash_bg)
        logoTV.loadImage(R.drawable.kolologo)
        signUpB.setOnClickListener {
            signUpPresenter.signUp(nameET.text.toString(),
                    emailET.text.toString(), phoneET.text.toString()
                    , passwordET.text.toString(), confirmPasswordET.text.toString())
        }
    }

    override fun onSuccess(user: UserResponse) {
        val loginPref = LoginPref(this)
        loginPref.setAccessToken((baseContext), user.token!!)
        user.userSignUp?.let {
            loginPref.saveUser(userSignUp = it)
        }
        startActivity(CityFragment.getStartIntent(this))
        loginPref.getNotificationToken()?.let {
            signUpPresenter.subscribeNotification(it)

        }
    }

    override fun onDestroy() {
        signUpPresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, SignUpActivity::class.java)
    }
}
