package com.hamza.rawaaprovider.ui.login

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.google.gson.JsonObject
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.UserSignUp

import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import javax.net.ssl.HttpsURLConnection


class SignInPresenter<V : SignInContract.View> : BasePresenter<V>(), SignInContract.Presenter<V> {

    private var dataManager: DataManager = AppDataManager()

    override fun signIn(phone: String?, password: String?) {

        if (checkLoginData(phone, password)) return

        if (mvpView?.isNetworkConnected()!!) {
            mvpView?.showLoading()
            val user = UserSignUp(phone = phone, password = password)
            dataManager.signIn(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<UserResponse>(mvpView!!) {

                        override fun onSuccess(t: UserResponse) {
                            mvpView?.hideLoading()
                            mvpView?.onSuccess(t)
                        }

                        override fun onError(e: Throwable) {
                            if (e is HttpException &&
                                    e.response().code() == HttpsURLConnection.HTTP_UNAUTHORIZED)
                                mvpView?.onError(R.string.error_username_password_incorrect)
                            else
                                super.onError(e)
                            mvpView?.hideLoading()
                        }
                    })
        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    override fun subscribeNotification(token: String) {
        val tokenObject = JsonObject()
        tokenObject.addProperty("token", token)
//        if (mvpView!!.isNetworkConnected())
//        dataManager.postSubscribeNotification(tokenObject)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(object : CallbackWrapper<Response<Void>>(mvpView!!) {
//
//                    override fun onSuccess(t: Response<Void>) {
//                        if (t.isSuccessful) {
//                            Log.d("test", "success ${t.body()}")
//                        } else
//                            Log.d("test", "fail ${t.errorBody()?.string()}")
//                    }
//                })
    }

    private fun checkLoginData(phone: String?, password: String?): Boolean {
        if ((phone == null || phone.isEmpty()) && (password == null || password.isEmpty())) {
            mvpView?.onError(R.string.empty_username_and_password)
            return true
        } else if (phone == null || phone.isEmpty()) {
            mvpView?.onError(R.string.empty_username)
            return true
        } else if (password == null || password.isEmpty()) {
            mvpView?.onError(R.string.empty_password)
            return true
        }
        return false
    }
}