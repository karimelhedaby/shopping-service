package com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseDialog
import com.hamza.solutions.kolo.R
import kotlinx.android.synthetic.main.dialog_details_order.*
import kotlinx.android.synthetic.main.dialog_details_order.view.*

class LocationDialog(var listener: okClickLoc) : BaseDialog() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_details_order, container, false)
        setUp(v)

        v.okBTN.setOnClickListener {
            if (locationET.text?.isEmpty()!!) {
                Toast.makeText(context, "Location is empty", Toast.LENGTH_LONG).show()
            } else {
                listener.onOkClickLoc(locationET.text.toString())
                dismiss()
            }
        }
        return v
    }

    override fun setUp(view: View?) {
    }

    companion object {
        fun newInstance(): LocationDialog {
            return newInstance()
        }

        interface okClickLoc {
            fun onOkClickLoc(location: String)
        }
    }

}