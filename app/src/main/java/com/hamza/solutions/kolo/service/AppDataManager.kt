package com.rawaa.hamza.rawaa.service

import com.hamza.solutions.kolo.App
import com.hamza.solutions.kolo.model.*
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import io.reactivex.Observable

/**
 * Created by Mohamed Fakhry on 23/03/2018.
 */
class AppDataManager : DataManager {
    override fun getAds(): Observable<ArrayList<AdsResponse>> {
        return App.getService.getAds()
    }


    override fun cancleOrder(orderId: String): Observable<CancleOrderResponse> {
        return App.getService.cancleOrder(orderId)
    }

    override fun cancleOfferOrder(orderId: String): Observable<CancleOfferOrderResponse> {
        return App.getService.cancleOfferOrder(orderId)
    }


    override fun getDeliveryPrice(): Observable<Delivery> {
        return App.getService.getDeliveryPrice()
    }

    override fun sendOrderOfferDone(offerId: String): Observable<OrderOfferDoneResponse> {
        return App.getService.sendOrderOfferDone(offerId)

    }

    override fun sendOrderDone(orderId: String): Observable<OrderDoneResponse> {
        return App.getService.sendOrderDone(orderId)

    }

    override fun getProductOrders(id: String): Observable<List<ProductsOrders>> {
        return App.getService.getProductOrders(id)
    }


    override fun orderProducts(order: Order): Observable<OrderResponse> {
        return App.getService.orderProducts(order)

    }


    override fun getOrderOffers(user: String): Observable<List<OrderOffer>> {
        return App.getService.getOrderOffers(user)
    }

    override fun orderOffer(order: OrderOfferRequest): Observable<OrderOffer> {
        return App.getService.orderOffer(order)
    }

    override fun getProfile(): Observable<Profile> {
        return App.getService.getProfile()
    }

    override fun getProducts(id: String): Observable<List<Product>> {
        return App.getService.getProducts(id)

    }

    override fun searchProducts(id: String, search_keyword: String): Observable<List<Product>> {
        return App.getService.searchProducts(id, search_keyword)
    }

    override fun getOffer(): Observable<List<Offer>> {
        return App.getService.getOffer()

    }

    override fun getClothsShopsByType(id: String, type: String , city : String): Observable<List<Shop>> {
        return App.getService.getClothsShopsByType(id, type,city)

    }

    override fun getShops(id: String, city: String): Observable<List<Shop>> {
        return App.getService.getShops(id, city)
    }

    override fun getCategory(): Observable<List<Category>> {
        return App.getService.getCategory()
    }

    override fun signIn(userSignUp: UserSignUp): Observable<UserResponse> {
        return App.getService.signIn(userSignUp)
    }

    override fun signUp(userSignUp: UserSignUp): Observable<UserResponse> {
        return App.getService.signUp(userSignUp)
    }

    override fun getCity(): Observable<List<City>> {
        return App.getService.getCity()
    }
}