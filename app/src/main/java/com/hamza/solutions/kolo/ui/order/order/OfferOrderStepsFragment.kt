package com.hamza.solutions.kolo.ui.order.order

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaaprovider.ui.register.OrderPresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Offer
import com.hamza.solutions.kolo.model.OrderOffer
import com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails.LocationDialog
import com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails.Quantitydialog
import com.hamza.solutions.kolo.ui.offers.OfferFragment
import com.hamza.solutions.kolo.ui.order.OrderContract
import kotlinx.android.synthetic.main.fragment_offer_info.*
import kotlinx.android.synthetic.main.fragment_offer_order_steps.*



class OfferOrderStepsFragment : BaseFragment(), OrderContract.View,
        LocationDialog.Companion.okClickLoc, Quantitydialog.Companion.okClick {

    var quantityOrder = ""
    var locationOrder = ""
    var quantityNumber = 0

    override fun getFragmentView(): Int = R.layout.fragment_offer_order_steps
    private lateinit var homePresenter: OrderContract.Presenter<OfferOrderStepsFragment>
    lateinit var offer: Offer

    override fun onOkClickLoc(location: String) {
        locationOrder = location
        orderlocationhTV.text = locationOrder

    }

    override fun onQuantityClick(quantity: String) {
        quantityOrder = quantity
        orderquantityTV.text = quantityOrder
    }

    override fun setUp(view: View?) {

        homePresenter = OrderPresenter()
        homePresenter.onAttach(this)
        offer = arguments?.getParcelable(OfferOrderStepsFragment.ARGUMENT_OFFER)!!

        priceET.text = "0 L.E "



        offerOrderNowBTN.setOnClickListener {

            if (locationOrder.equals("")) {
                Toast.makeText(context, getString(R.string.must_enter_location), Toast.LENGTH_LONG).show()
            } else if (quantityNumber == 0) {
                Toast.makeText(context, getString(R.string.must_enter_number), Toast.LENGTH_LONG).show()
            } else {

                homePresenter.sendOrder(offer.id, quantityNumber, locationOrder)

                Toast.makeText(context, getString(R.string.success_send_order), Toast.LENGTH_SHORT).show()
                offerOrderNowBTN.visibility = View.GONE

                baseActivity?.replaceFragmentToActivity(OfferFragment.newInstance(), true)

            }
        }

        quantityofferPicker.setClickNumberPickerListener { _, currentValue, _ ->
            if (currentValue.equals(0)) {
                totalPriceTV.text = "0 L.E"
            } else {
                quantityNumber = currentValue.toInt()
                // Toast.makeText(context,offer.pricepostDisc.toDouble().times(currentValue).toString(),Toast.LENGTH_LONG).show()
                priceET.text = offer.pricepostDisc.toDouble().times(currentValue).toString()
            }

        }

        setOfferOrderLocationBTN.setOnClickListener {
            val pop = LocationDialog(this)
            val fm = this@OfferOrderStepsFragment.fragmentManager
            fm?.let {
                pop.show(it, "Address")
            }
        }

        /*  setQuantityBTN.setOnClickListener {

              val pop = Quantitydialog(this)
              val fm = this@OfferOrderStepsFragment.fragmentManager
              fm?.let { pop.show(it, "Quality") }

      }*/
    }
    override fun onOrderSend(orderOffer: OrderOffer) {

        // Timber.d(orderOffer.toString() + "")
        Toast.makeText(context, getString(R.string.success_send_order), Toast.LENGTH_LONG).show()
        offerOrderNowBTN.visibility = View.GONE

    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {

        private const val ARGUMENT_OFFER = "ARGUMENT_OFFER"

        fun newInstance(offer: Offer): OfferOrderStepsFragment {
            val offerOrderStepsFragment = OfferOrderStepsFragment()
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_OFFER, offer)
            offerOrderStepsFragment.arguments = bundle

            // Timber.d(offer.toString())
            return offerOrderStepsFragment

        }
    }
}