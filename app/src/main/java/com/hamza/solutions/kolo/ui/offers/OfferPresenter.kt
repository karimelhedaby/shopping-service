package com.hamza.solutions.kolo.ui.offers

import android.util.Log
import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Offer
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



/**
 * Created by karim on 7/18/18.
 */
class OfferPresenter<V : OfferContract.View> : BasePresenter<V>(), OfferContract.Presenter<V> {

    override fun getOffers() {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.getOffer()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Offer>>(mvpView!!) {
                        override fun onSuccess(t: List<Offer>) {
                            mvpView?.hideLoading()
                            mvpView?.onOffersRecevied(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }
                    })

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    private var dataManager: DataManager = AppDataManager()

}