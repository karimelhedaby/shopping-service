package com.hamza.solutions.kolo.ui.home

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.CategoriesContract
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager


class CategoriesPresenter<V : CategoriesContract.View> : BasePresenter<V>(), CategoriesContract.Presenter<V> {

    private var dataManager: DataManager = AppDataManager()


}