package com.hamza.solutions.kolo.ui.order.order

import android.app.Activity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import br.com.joinersa.oooalertdialog.Animation
import br.com.joinersa.oooalertdialog.OnClickListener
import br.com.joinersa.oooalertdialog.OoOAlertDialog
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.*
import com.rawaa.hamza.rawaa.ui.cart.order.details.OrderDetailsContract
import com.rawaa.hamza.rawaa.ui.cart.order.details.OrderDetailsPresenter
import kotlinx.android.synthetic.main.fragment_order_info.*



class OrderDetailsFragment : BaseFragment(), OrderDetailsContract.View {
    override fun onOrderOfferCancled(cancleOfferOrderResponse: CancleOfferOrderResponse) {}

    override fun onOrderOfferDone(offerDone: OrderOfferDoneResponse) {}

    override fun onOrderDone(doneOrder: OrderDoneResponse) {

        // Timber.d(doneOrder.toString())
        Toast.makeText(context, getString(R.string.thanks_for_service), Toast.LENGTH_LONG).show()
        doneBTN.visibility = View.GONE

    }


    override fun oncancleOrder(cancleOrderResponse: CancleOrderResponse) {

        Toast.makeText(context, getString(R.string.order_succ), Toast.LENGTH_LONG).show()

    }

    lateinit var presenter: OrderDetailsContract.Presenter<OrderDetailsFragment>
    private lateinit var order: ProductsOrders
    var products = mutableListOf<ProductQuantity>()

    override fun getFragmentView(): Int = R.layout.fragment_order_info

    override fun setUp(view: View?) {

        presenter = OrderDetailsPresenter()
        presenter.onAttach(this)


        order = arguments?.getParcelable(ORDER_OFFERKEY)!!
        with(order) {
            itemsCountTV.text = productDetails.size.toString()
            priceTV.text = "$productPrice L.E"
            deliveryPriceTV.text = "$deliveryPrice L.E"

            products = productDetails
                    .filter { it.product.imgs.isNotEmpty() }
                    .toMutableList()

            productsCV.setViewListener { position ->
                val customView = activity?.layoutInflater?.inflate(R.layout.item_product_carousel, null)
                val product = products[position].product
                with(product) {
                    customView?.findViewById<ImageView>(R.id.productIV).loadImage(imgs[0])
                    customView?.findViewById<TextView>(R.id.productTV)?.text = title
                    customView?.findViewById<TextView>(R.id.priceTV)?.text = "$price L.E"
                }
                customView
            }
            productsCV.pageCount = products.size

            val total = productPrice + deliveryPrice
            totalPriceTV.text = "$total L.E"

            doneBTN.setOnClickListener {

                OoOAlertDialog.Builder(context as Activity?)
                        .setTitle(getString(R.string.u_order))
                        .setMessage(getString(R.string.order_done_btn))
                        .setAnimation(Animation.POP)
                        .setPositiveButtonColor(R.color.colorAccent)
                        .setNegativeButtonColor(R.color.colorAccent)
                        .setNegativeButtonTextColor(R.color.whiteColor)
                        .setPositiveButtonTextColor(R.color.whiteColor)
                        .setTitleColor(R.color.colorAccent)
                        .setPositiveButton(getString(R.string.yes), OnClickListener {
                            presenter.orderDone(orderId = id)


                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .build();
            }

            cancleBTN.setOnClickListener {
                OoOAlertDialog.Builder(context as Activity?)
                        .setTitle(getString(R.string.cncl_order))
                        .setMessage(getString(R.string.need_cnl_order))
                        .setAnimation(Animation.POP)
                        .setPositiveButtonColor(R.color.colorAccent)
                        .setNegativeButtonColor(R.color.colorAccent)
                        .setNegativeButtonTextColor(R.color.whiteColor)
                        .setPositiveButtonTextColor(R.color.whiteColor)
                        .setTitleColor(R.color.colorAccent)
                        .setPositiveButton(getString(R.string.yes), OnClickListener {

                            presenter.cancleOrder(order.id)
                            cancleBTN.visibility = View.GONE
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .build();

            }

            if (status.equals("Pindding")) {
                cancleBTN.visibility = View.VISIBLE
            }

            if (status.equals("Done")) {
                doneBTN.visibility = View.GONE
                statusL.visibility = View.GONE
                doneL.visibility = View.VISIBLE
            }

            if (status.equals("OnTheWay")) {
                doneBTN.visibility = View.VISIBLE
            }

            if (status.equals("Cancle")) {
                statusL.visibility = View.GONE
                cancelL.visibility = View.VISIBLE
            }

            when (status) {
                "Pindding" -> {
                    shoppingDateIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_cart) }))
                }
                "Accepted" -> {
                    verifiedIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_verify) }))
                }
                "OnTheWay" -> {
                    deliveringIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_shopping_delivering) }))
                }
                "Done" -> {
                    deliveredIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_delivered) }))
                }
            }
        }
    }

    companion object {
        private val ORDER_OFFERKEY = "ORDER_OFFERKEY"

        fun newInstance(currentorder: ProductsOrders): OrderDetailsFragment {
            val orderdetailsFragment = OrderDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable(ORDER_OFFERKEY, currentorder)
            orderdetailsFragment.arguments = bundle
            return orderdetailsFragment
        }
    }
}