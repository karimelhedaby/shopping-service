package com.hamza.solutions.kolo

import android.support.multidex.MultiDexApplication
import com.androidnetworking.interceptors.HttpLoggingInterceptor
import com.rawaa.hamza.rawaaprovider.service.retrofit.AuthInterceptor
import com.rawaa.hamza.rawaaprovider.service.retrofit.Constant
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import com.rawaa.hamza.rawaaprovider.service.retrofit.Service
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import uk.co.chrisjenx.calligraphy.CalligraphyConfig


class App : MultiDexApplication() {

    override fun onCreate() {
        super.onCreate()
        val loginPref = this.let { LoginPref(it) }

        createApi(AuthInterceptor(loginPref.accessToken))
//        initCalligraphyConfig()

//        if (BuildConfig.DEBUG) {
//            Timber.plant(DebugTree())
//        }
    }

    private fun initCalligraphyConfig() {
        CalligraphyConfig.initDefault(CalligraphyConfig.Builder()
//                .setDefaultFontPath(getString(R.string.font_name))
                .setFontAttrId(R.attr.fontPath)
                .build())
    }

    companion object {

        lateinit var getService: Service

        internal fun createApi(authInterceptor: AuthInterceptor?) {
            val clientBuilder: OkHttpClient.Builder
            val client: OkHttpClient
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BASIC

            clientBuilder = OkHttpClient.Builder()
                    .addInterceptor(interceptor)

            authInterceptor?.let {
                clientBuilder.addInterceptor(
                        authInterceptor
                )
            }

            client = clientBuilder.build()

            val retrofit = Retrofit.Builder()
                    .baseUrl(Constant.BASE_URL)
                    .client(client)
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()

            getService = retrofit.create(Service::class.java)
        }
    }
}
