package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Product(
        @SerializedName("imgs") val imgs: List<String>,
        @SerializedName("avaliablesizes") val avaliablesizes: List<String>,
        @SerializedName("avaliablecolors") val avaliablecolors: List<String>,
        @SerializedName("id", alternate = ["_id"]) val id: String,
        @SerializedName("title") val title: String,
        @SerializedName("price") val price: String,
        @SerializedName("marketId") val marketId: MarketId,
        @SerializedName("desc") val desc: String,
        @SerializedName("creationDate") val creationDate: String,
        var quantity: Int = 0
) : Parcelable

@Parcelize
data class ProductResponse(
        @SerializedName("imgs") val imgs: List<String>,
        @SerializedName("avaliablesizes") val avaliablesizes: List<String>,
        @SerializedName("avaliablecolors") val avaliablecolors: List<String>,
        @SerializedName("id", alternate = ["_id"]) val id: String,
        @SerializedName("title") val title: String,
        @SerializedName("price") val price: String,
        @SerializedName("marketId") val marketId: String,
        @SerializedName("desc") val desc: String,
        @SerializedName("creationDate") val creationDate: String,
        var quantity: Int = 0
) : Parcelable

@Parcelize
data class ProductQuantity(
        var product: ProductResponse,
        var quantity: Int = 0
) : Parcelable