package com.hamza.solutions.kolo.ui.home

import android.support.v7.widget.LinearLayoutManager
import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaa.ui.home.HomeContract
import com.hamza.rawaaprovider.ui.home.CategoriesFragment
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Category
import com.hamza.solutions.kolo.ui.clothes.shops.ShopsFragment
import com.hamza.solutions.kolo.ui.home.adapter.CatougryAdapter
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseFragment(), HomeContract.View, CatougryAdapter.IndecatorInterface {

    override fun getFragmentView(): Int = R.layout.fragment_home

    private lateinit var homePresenter: HomeContract.Presenter<HomeFragment>

//    lateinit var user: UserApp

    override fun setUp(view: View?) {
        homePresenter = HomePresenter()
        homePresenter.onAttach(this)

        homePresenter.getCategory()
        homePresenter.getAds()


        val loginPref = context?.let { LoginPref(it) }
//        user = loginPref?.getUser()!!
    }


    override fun onCategoryRecived(categoryResponse: List<Category>) {
        // Timber.d(categoryResponse.toString())
        catougryRecyclerView.layoutManager = LinearLayoutManager(context)
        catougryRecyclerView.adapter = context?.let { CatougryAdapter(it, categoryResponse, this) }
    }

    override fun onCatogryItemClickListener(catogry: Category) {

        when {
            catogry.categoryIdentity == "cloths" -> baseActivity?.replaceFragmentToActivity(CategoriesFragment.newInstance(catogry), true)
            catogry.categoryIdentity == "grocery" -> baseActivity?.replaceFragmentToActivity(ShopsFragment.newInstance(catogry, ""), true)
            else -> baseActivity?.replaceFragmentToActivity(ShopsFragment.newInstance(catogry, ""), true)
        }

    }

    override fun onAdsResponse(ads: ArrayList<AdsResponse>) {
        if (ads.size != 0) {
            activity?.let {
                adsCV.setViewListener(CarouselViewListener(it, ads))
                adsCV.pageCount = ads.size
            }
        }
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun newInstance(): HomeFragment = HomeFragment()
    }
}