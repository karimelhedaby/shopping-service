package com.hamza.solutions.kolo.ui.splash

import android.content.res.Configuration
import android.os.Bundle
import android.os.Handler
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.city.CityFragment
import com.hamza.solutions.kolo.ui.login.SignInActivity
import com.hamza.solutions.kolo.ui.welcome.WelcomeActivity
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.activity_splash.*
import java.util.*


class SplashActivity : BaseActivity() {

    override fun getActivityView(): Int = R.layout.activity_splash

    override fun afterInflation(savedInstance: Bundle?) {
        backgroundIV.loadImage(R.drawable.splash_bg)

        logo.loadImage(R.drawable.kolologo)
        Handler().postDelayed({


            if (LoginPref(this).getLang() == "en") {
                val locale = Locale("en")
                Locale.setDefault(locale)
                val config = Configuration()
                config.locale = locale
                config.fontScale = 0.90.toFloat()
                this!!.getResources().updateConfiguration(config, this!!.getResources().getDisplayMetrics())
            } else if (LoginPref(this).getLang() == "ar") {
                val locale = Locale("ar")
                Locale.setDefault(locale)
                val config = Configuration()
                config.locale = locale
                config.fontScale = 0.90.toFloat()
                this!!.getResources().updateConfiguration(config, this!!.getResources().getDisplayMetrics())

            }


            ViewCompat.getTransitionName(logoTV)?.let {
                ActivityOptionsCompat.makeSceneTransitionAnimation(this@SplashActivity,
                        logoTV,
                        it)
            }

            val loginPref = LoginPref(this.applicationContext)

            startActivity(if (loginPref.getUser() != null) {
                loginPref.setSecureConnection()
                CityFragment.getStartIntent(this)
            } else
                WelcomeActivity.getStartIntent(this)
            )

            finish()
        }, 3000)
    }
}