package com.hamza.solutions.kolo.ui.clothes.products.details

import android.app.Activity
import android.app.PendingIntent.getActivity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.rawaa.ui.home.ProductDetailsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Product
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.home.ProductDetailsPresenter
import kotlinx.android.synthetic.main.fragment_product_details.*

import android.support.v7.widget.LinearLayoutManager
import com.hamza.solutions.kolo.R.id.colorRV
import com.hamza.solutions.kolo.ui.clothes.products.details.adapter.color.ColorAdapter
import com.hamza.solutions.kolo.ui.clothes.products.details.adapter.size.SizeAdapter
import kotlinx.android.synthetic.main.fragment_product_details.view.*


class ProductDetailsFragment : BaseActivity(), ProductDetailsContract.View {

    private lateinit var homePresenter: ProductDetailsContract.Presenter<ProductDetailsFragment>

    override fun getActivityView(): Int = R.layout.fragment_product_details

    lateinit var product: Product


    override fun afterInflation(savedInstance: Bundle?) {
        homePresenter = ProductDetailsPresenter()
        homePresenter.onAttach(this)


        product = intent.getParcelableExtra(EXTRA_PRODUCT)!!

        productCV.setViewListener(this.let { CarouselViewListener(it, product.imgs) })
        productCV.pageCount = product.imgs.size

        if (product.avaliablecolors.isEmpty() || product.avaliablecolors == null) {
            colAndSizView.visibility = View.GONE
            colorsRL.visibility = View.GONE
        }
        if (product.avaliablesizes.isEmpty() || product.avaliablesizes == null) {
            sizesRL.visibility = View.GONE
            colAndSizView.visibility = View.GONE
        } else {
            colAndSizView.visibility = View.VISIBLE
            colorsRL.visibility = View.VISIBLE
            sizesRL.visibility = View.VISIBLE
        }
        with(product) {
            productNameTV.text = title
            priceTV.text = "$price L.E"
            descTV.text = desc

        }

        // Timber.d(product.avaliablecolors.toString())
        colorRV.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        colorRV.adapter = ColorAdapter(product.avaliablecolors as MutableList<String>)

        sizeRV.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
        sizeRV.adapter = SizeAdapter(product.avaliablesizes as MutableList<String>)

        productDetailsLL.setOnClickListener {
            detailsL.visibility = if (detailsL.visibility == View.GONE) {
                plusIC.loadImage(R.drawable.ic_minus_black_48dp)
                View.VISIBLE
            } else {
                plusIC.loadImage(R.drawable.ic_add_black_48dp)
                View.GONE
            }
        }

    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        private val TRANSITION_NAME = "TRANSITION_NAME"
        private val ARGUMENT_PRODUCT = "ARGUMENT_PRODUCT"


        val EXTRA_PRODUCT = "EXTRA_PRODUCT"

        fun getStartIntent(context: Context, product: Product): Intent {
            var intent = Intent(context, ProductDetailsFragment::class.java)
            // Timber.d(product.toString() + " POOOO")
            intent.putExtra(EXTRA_PRODUCT, product)
            return intent
        }
    }
}