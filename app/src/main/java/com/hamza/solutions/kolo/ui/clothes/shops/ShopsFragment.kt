package com.hamza.solutions.kolo.ui.clothes.shops

import android.os.Build
import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaa.ui.home.ShopsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Category
import com.hamza.solutions.kolo.model.Shop
import com.hamza.solutions.kolo.ui.clothes.products.ProductsFragment
import com.hamza.solutions.kolo.ui.clothes.shops.adapter.ShopAdapter
import com.hamza.solutions.kolo.ui.home.CarouselViewListener
import com.hamza.solutions.kolo.ui.home.ShopsPresenter
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_shops.*

class ShopsFragment : BaseFragment(), ShopsContract.View, ShopAdapter.shopListener {

    override fun getFragmentView(): Int = R.layout.fragment_shops
    private lateinit var homePresenter: ShopsContract.Presenter<ShopsFragment>
    private lateinit var loginPref: LoginPref

    override fun setUp(view: View?) {
        loginPref = baseActivity?.let { LoginPref(it) }!!
        val city = loginPref?.getCity()

        homePresenter = ShopsPresenter()
        homePresenter.onAttach(this)
        homePresenter.getAds()

        if (!typeOfCloths.equals("")) {
            homePresenter.getClothsShopByType(category.id, typeOfCloths, city?.id!!)
        } else {
            homePresenter.getShops(category.id, city!!.id)
        }
    }

    override fun onShopsRecevied(shops: List<Shop>) {
        shopsRV.layoutManager = GridLayoutManager(context, 2)
        shopsRV.addItemDecoration(
                GridSpacingItemDecoration(2, ViewUtils.dpToPx(8F), true, 0)
        )
        shopsRV.adapter = context?.let { ShopAdapter(it, shops, this) }
    }

    override fun onClothsShopTypeRecevied(shops: List<Shop>) {
        shopsRV.layoutManager = GridLayoutManager(context, 2)
        shopsRV.addItemDecoration(
                GridSpacingItemDecoration(2, ViewUtils.dpToPx(8F), true, 0)
        )
        shopsRV.adapter = context?.let { ShopAdapter(it, shops, this) }
    }

    override fun onShopClickListener(shop: Shop, position: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            transitionName = "shop_logo_transition_$position"
        }
        baseActivity?.replaceFragmentToActivity(ProductsFragment.newInstance(transitionName, shop), true)
    }

    override fun onAdsResponse(ads: ArrayList<AdsResponse>) {

        if (ads.size != 0) {
            activity?.let {
                adsShopsCV.setViewListener(activity?.let { CarouselViewListener(it, ads) })
                adsShopsCV.pageCount = ads.size
            }
        }
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {

        private lateinit var category: Category
        private lateinit var typeOfCloths: String
        lateinit var transitionName: String

        fun newInstance(catogry: Category, type: String): ShopsFragment {
            category = catogry
            typeOfCloths = type

            return ShopsFragment()
        }
    }
}
