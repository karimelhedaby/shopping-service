package com.hamza.solutions.kolo.ui.order.info

import android.app.Activity
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import android.widget.Toast
import br.com.joinersa.oooalertdialog.Animation
import br.com.joinersa.oooalertdialog.OnClickListener
import br.com.joinersa.oooalertdialog.OoOAlertDialog
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.loadImage
import com.fivehundredpx.android.blur.BlurringView
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.*
import com.rawaa.hamza.rawaa.ui.cart.order.details.OrderDetailsContract
import com.rawaa.hamza.rawaa.ui.cart.order.details.OrderDetailsPresenter
import kotlinx.android.synthetic.main.fragment_offer_info.*



class OrderOfferDetailsFragment : BaseFragment(), OrderDetailsContract.View {


    override fun oncancleOrder(cancleOrderResponse: CancleOrderResponse) {}


    override fun onOrderDone(doneOrder: OrderDoneResponse) {

    }

    lateinit var presenter: OrderDetailsContract.Presenter<OrderOfferDetailsFragment>
    private lateinit var orderoffer: OrderOffer
    private lateinit var blur: BlurringView


    override fun getFragmentView(): Int = R.layout.fragment_offer_info

    override fun onOrderOfferCancled(cancleOfferOrderResponse: CancleOfferOrderResponse) {

        Toast.makeText(context,"Order is successfuly cancled",Toast.LENGTH_LONG).show()
        cancleBTN.visibility = View.GONE
    }

    override fun setUp(view: View?) {

        presenter = OrderDetailsPresenter()
        presenter.onAttach(this)
        orderoffer = arguments?.getParcelable(ORDER_OFFERKEY)!!
        with(orderoffer) {
            itemsCountTV.text = quantity
            priceTV.text = "$price L.E"
            deliveryPriceTV.text = "$dliveryPrice L.E"
            offers.imgs[0]?.let { offerIV.loadImage(it) }
            totalPriceTV.text = "$total L.E"

            doneBTN.setOnClickListener {

                OoOAlertDialog.Builder(context as Activity?)
                        .setTitle(getString(R.string.your_order))
                        .setMessage(getString(R.string.order_is_done_dialog))
                        .setAnimation(Animation.POP)
                        .setPositiveButtonColor(R.color.green_dark)
                        .setNegativeButtonColor(R.color.red)
                        .setNegativeButtonTextColor(R.color.whiteColor)
                        .setPositiveButtonTextColor(R.color.whiteColor)
                        .setTitleColor(R.color.colorAccent)
                        .setPositiveButton(getString(R.string.yes), OnClickListener {
                            presenter.orderOfferDone(id)

                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .build();
            }

            cancleBTN.setOnClickListener {

                OoOAlertDialog.Builder(context as Activity?)
                        .setTitle(getString(R.string.cancle_order))
                        .setMessage(getString(R.string.cancle_order_dialog))
                        .setAnimation(Animation.POP)
                        .setPositiveButtonColor(R.color.colorAccent)
                        .setNegativeButtonColor(R.color.colorAccent)
                        .setNegativeButtonTextColor(R.color.whiteColor)
                        .setPositiveButtonTextColor(R.color.whiteColor)
                        .setTitleColor(R.color.colorAccent)
                        .setPositiveButton(getString(R.string.yes), OnClickListener {
                            presenter.orderOfferCancle(orderoffer.id)
                            cancleBTN.visibility = View.GONE
                        })
                        .setNegativeButton(getString(R.string.no), null)
                        .build();

            }





            if (status.equals("Pindding")) {
                cancleBTN.visibility = View.VISIBLE
            }

            if (status.equals("OnTheWay")) {
                doneBTN.visibility = View.VISIBLE
            }

            if (status.equals("Done")) {
                doneBTN.visibility = View.GONE
                statusL.visibility = View.GONE
                doneL.visibility = View.VISIBLE
            }

            if (status.equals("Delivered")) {
                doneBTN.visibility = View.VISIBLE
            }

            if(status.equals("Cancle")){
                statusL.visibility = View.GONE
                cancelL.visibility = View.VISIBLE
            }


            when (status) {
                "Pindding" -> {
                    shoppingDateIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_cart) }))
                }
                "Accepted" -> {
                    verifiedIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_verify) }))
                }
                "OnTheWay" -> {
                    deliveringIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_shopping_delivering) }))
                }
                "Done" -> {
                    deliveredIV.setImageDrawable((context?.let { ContextCompat.getDrawable(it, R.drawable.ic_delivered) }))
                }
            }

        }
    }

    override fun onOrderOfferDone(offerDone: OrderOfferDoneResponse) {
        // Timber.d(offerDone.toString())
        Toast.makeText(context, getString(R.string.thanks), Toast.LENGTH_LONG).show()
        doneBTN.visibility = View.GONE

    }


    companion object {
        private val ORDER_OFFERKEY = "ORDER_OFFERKEY"

        fun newInstance(currentorder: OrderOffer): OrderOfferDetailsFragment {
            val orderdetailsFragment = OrderOfferDetailsFragment()
            val bundle = Bundle()
            bundle.putParcelable(ORDER_OFFERKEY, currentorder)
            orderdetailsFragment.arguments = bundle
            return orderdetailsFragment
        }
    }

}