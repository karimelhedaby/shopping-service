package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class OrderOfferRequest(
        @field:SerializedName("offers")
        val offerId: String? = null,

        @field:SerializedName("quantity")
        val quantity: Int? = null,

        @field:SerializedName("address")
        val address: String? = null
)