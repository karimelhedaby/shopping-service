package com.hamza.solutions.kolo.ui.clothes.shops.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Shop
import kotlinx.android.synthetic.main.item_shop.view.*

class ShopAdapter(var context: Context, var shops: List<Shop>, var shListener: shopListener) : RecyclerView.Adapter<ShopAdapter.ShopVH>() {


    class ShopVH(itemView: View) : RecyclerView.ViewHolder(itemView) {


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShopVH {
        return ShopVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_shop, null))
    }

    override fun onBindViewHolder(holder: ShopVH, position: Int) {
        with(holder.itemView) {


            Glide.with(context)
                    .load(shops.get(position).logo)
                    .into(shopCoverIV)

            shopTitleTV.text = shops.get(position).nameOfShop

            holder.itemView.setOnClickListener {
                shListener.onShopClickListener(shops.get(position), position)
            }


        }
    }

    open interface shopListener {

        fun onShopClickListener(shop: Shop, position: Int)
    }

    override fun getItemCount(): Int {
        return shops.size
    }
}


