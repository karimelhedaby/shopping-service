package com.hamza.solutions.kolo.ui.offers

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Offer

/**
 * Created by karim on 7/18/18.
 */
interface OfferContract {
    interface View : MvpView{
        fun onOffersRecevied(offers :List<Offer>)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getOffers()
    }

}