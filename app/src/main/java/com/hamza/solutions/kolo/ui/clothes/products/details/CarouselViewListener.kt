package com.hamza.solutions.kolo.ui.clothes.products.details

import android.app.Activity
import android.view.View
import android.widget.ImageView
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.synnapps.carouselview.ViewListener

class CarouselViewListener(var activity: Activity, var productImages : List<String>) : ViewListener {

    var images: List<String> = productImages

    override fun setViewForPosition(position: Int): View {
        val customView = activity.layoutInflater.inflate(R.layout.item_product_carousel_view, null)
        with(customView) {
            this.findViewById<ImageView>(R.id.productIV).loadImage(images[position])
        }
        return customView
    }
}