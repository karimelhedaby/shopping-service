package com.hamza.rawaaprovider.ui.home

import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.loadImage
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaa.ui.home.CategoriesContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Category
import com.hamza.solutions.kolo.ui.clothes.shops.ShopsFragment
import com.hamza.solutions.kolo.ui.home.CategoriesPresenter
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_categoties.*

class CategoriesFragment : BaseFragment(), CategoriesContract.View {


    override fun getFragmentView(): Int = R.layout.fragment_categoties

    private lateinit var homePresenter: CategoriesContract.Presenter<CategoriesFragment>

    override fun setUp(view: View?) {
        homePresenter = CategoriesPresenter()
        homePresenter.onAttach(this)

        menClothesIV.loadImage(R.drawable.men_clothes)
        womenClothesIV.loadImage(R.drawable.women_clothes)
        kidsClothesIV.loadImage(R.drawable.kids_clothes)

        menC.setOnClickListener {
            baseActivity?.replaceFragmentToActivity(ShopsFragment.newInstance(category, "male"), true)
        }

        womenClothesIV.setOnClickListener {

            baseActivity?.replaceFragmentToActivity(ShopsFragment.newInstance(category, "female"), true)
        }

        kidsClothesIV.setOnClickListener {

            baseActivity?.replaceFragmentToActivity(ShopsFragment.newInstance(category, "kids"), true)
        }

        val loginPref = context?.let { LoginPref(it) }
    }


    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        lateinit var category: Category
        fun newInstance(catogry: Category): CategoriesFragment {
            category = catogry

            return CategoriesFragment()
        }
    }
}