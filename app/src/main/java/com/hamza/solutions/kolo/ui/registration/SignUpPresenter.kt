package com.hamza.rawaaprovider.ui.register

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.example.computec.eltadreb.utils.CommonUtils.isEmailValid
import com.google.gson.JsonObject
import com.hamza.rawaa.ui.register.SignUpContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.UserSignUp
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SignUpPresenter<V : SignUpContract.View> : BasePresenter<V>(), SignUpContract.Presenter<V> {

    var dataManager: DataManager = AppDataManager()

    override fun signUp(name: String?, email: String?,
                        phone: String?, password: String?, confirmPassword: String?) {

        if (checkUserData(name, email, password, phone, confirmPassword)) return

        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            val user = UserSignUp(name = name, email = email, phone = phone, password = password)

            dataManager.signUp(user)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<UserResponse>(mvpView!!) {
                        override fun onSuccess(user: UserResponse) {
                            if (isViewAttached()) {
                                mvpView?.hideLoading()
                                mvpView?.onSuccess(user)
                                mvpView?.showMessage(R.string.sign_up_successfully)
                            }
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            mvpView!!.hideLoading()
                        }
                    })


        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    override fun subscribeNotification(token: String) {
        val tokenObject = JsonObject()
        tokenObject.addProperty("token", token)
//        if (mvpView!!.isNetworkConnected())
//        dataManager.postSubscribeNotification(tokenObject)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribeWith(object : CallbackWrapper<Response<Void>>(mvpView!!) {
//
//                    override fun onSuccess(t: Response<Void>) {
//                        if (t.isSuccessful) {
//                            Log.d("test", "success ${t.body()}")
//                        } else
//                            Log.d("test", "fail ${t.errorBody()?.string()}")
//                    }
//                })
    }

    private fun checkUserData(name: String?, email: String?, password: String?,
                              phone: String?, confirmPassword: String?): Boolean {
        if ((name == null || name.isEmpty()) &&
                (password == null || password.isEmpty()) &&
                (phone == null || phone.isEmpty()) &&
                (confirmPassword == null || confirmPassword.isEmpty())) {
            mvpView?.onError(R.string.empty_username_and_password)
            return true
        } else if (name == null || name.isEmpty()) {
            mvpView?.onError(R.string.empty_username)
            return true
        } else if (email == null || email.isEmpty()) {
            mvpView?.onError(R.string.empty_email)
            return true
        } else if (!isEmailValid(email)) {
            mvpView?.onError(R.string.email_not_vailed)
            return true
        } else if (password == null || password.isEmpty()) {
            mvpView?.onError(R.string.empty_password)
            return true
        } else if ((phone == null || phone.isEmpty())) {
            mvpView?.onError(R.string.empty_phone)
            return true
        } else if ((confirmPassword == null || confirmPassword.isEmpty())) {
            mvpView?.onError(R.string.empty_confirm_password)
            return true
        } else if (password != confirmPassword) {
            mvpView?.onError(R.string.error_password_not_equal_confirm_password)
            return true
        }
        return false
    }
}