package com.hamza.solutions.kolo.ui.home.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Category
import kotlinx.android.synthetic.main.catougry_item.view.*

class CatougryAdapter(var context: Context, var categores: List<Category>, var catogryListner: IndecatorInterface)
    : RecyclerView.Adapter<CatougryAdapter.CatougryVH>() {

    class CatougryVH(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var categoryImage = itemView.categoryIV
        var categoryName = itemView.catougryNameTV
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CatougryVH {
        return CatougryVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.catougry_item, null))
    }

    override fun onBindViewHolder(holder: CatougryVH, position: Int) {
        Glide.with(context)
                .load(categores.get(position).image)
                .into(holder.categoryImage)

        holder.categoryName.text = categores.get(position).name

        holder.itemView.setOnClickListener {
            catogryListner.onCatogryItemClickListener(categores.get(position))
        }

    }

    open interface IndecatorInterface {

        fun onCatogryItemClickListener(catogry: Category)
    }

    override fun getItemCount(): Int {
        return categores.size
    }
}
