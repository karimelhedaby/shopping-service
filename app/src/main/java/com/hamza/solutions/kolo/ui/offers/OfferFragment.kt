package com.hamza.solutions.kolo.ui.offers

import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Offer
import com.hamza.solutions.kolo.ui.offers.adapter.OfferAdapter
import com.hamza.solutions.kolo.ui.order.order.OfferOrderStepsFragment
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import kotlinx.android.synthetic.main.fragment_offer.*

class OfferFragment : BaseFragment(), OfferContract.View, OfferAdapter.offerOrderClick {

    private lateinit var homePresenter: OfferContract.Presenter<OfferFragment>

    override fun getFragmentView(): Int = R.layout.fragment_offer

    override fun setUp(view: View?) {
        homePresenter = OfferPresenter()
        homePresenter.onAttach(this)
        homePresenter.getOffers()
    }

    override fun onOffersRecevied(offers: List<Offer>) {
        offerRV.layoutManager = GridLayoutManager(context, 1)
        offerRV.addItemDecoration(
                GridSpacingItemDecoration(1, ViewUtils.dpToPx(8F), true, 0)
        )
        offerRV.adapter = context?.let { OfferAdapter(it, offers, this) }
    }

    override fun onOfferOrderClickListener(offer: Offer) {

        baseActivity?.replaceFragmentToActivity(OfferOrderStepsFragment.newInstance(offer), true)
//
//        val pop = OrderDialog.newInstance(offer)
//        val fm = this@OfferFragment.fragmentManager
//        fm?.let { pop.show(it, "name") }
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun newInstance(): OfferFragment = OfferFragment()
    }


}