package com.hamza.solutions.kolo.ui.home

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.HomeContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Category
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class HomePresenter<V : HomeContract.View> : BasePresenter<V>(), HomeContract.Presenter<V> {
    private var dataManager: DataManager = AppDataManager()

    override fun getAds() {
        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            dataManager.getAds()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<ArrayList<AdsResponse>>(mvpView!!) {
                        override fun onSuccess(t: ArrayList<AdsResponse>) {
                            mvpView?.hideLoading()
                            mvpView?.onAdsResponse(t)
                        }
                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    override fun getCategory() {
        // Timber.d("test send")
        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

            mvpView?.showLoading()
            dataManager.getCategory()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Category>>(mvpView!!) {
                        override fun onSuccess(t: List<Category>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onCategoryRecived(t)
                        }
                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }
}