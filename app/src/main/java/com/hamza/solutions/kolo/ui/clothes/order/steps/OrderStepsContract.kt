package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Delivery
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.OrderResponse

interface OrderStepsContract {

    interface View : MvpView {
        fun onOrderSend(orderResponse: OrderResponse)
        fun onDeliveryPriceRecived(delivery: Delivery)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getDelivery()
        fun sendOrder(order: Order)

    }
}