package com.hamza.solutions.kolo.ui.login

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.ActivityOptionsCompat
import android.support.v4.view.ViewCompat
import android.util.Log
import com.example.computec.eltadreb.utils.loadImage
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaaprovider.ui.login.SignInContract
import com.hamza.rawaaprovider.ui.login.SignInPresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.city.CityFragment
import com.hamza.solutions.kolo.ui.main.MainActivity
import com.hamza.solutions.kolo.ui.registration.SignUpActivity
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.activity_sign_in.*
import android.support.v7.app.AppCompatDelegate




class SignInActivity : BaseActivity(), SignInContract.View {

    private lateinit var signInPresenter: SignInContract.Presenter<SignInActivity>

    override fun getActivityView(): Int = R.layout.activity_sign_in

    override fun afterInflation(savedInstance: Bundle?) {
        signInPresenter = SignInPresenter()
        signInPresenter.onAttach(this)
        logoTV.loadImage(R.drawable.kolologo)

        backgroundIV.loadImage(R.drawable.splash_bg)
//        logoIV.loadImage(R.drawable.logo)

        signInB.setOnClickListener {
            signInPresenter.signIn(phoneET.text.toString(), passwordET.text.toString())
        }

        signUpL.setOnClickListener {
            val options = ViewCompat.getTransitionName(logoTV)?.let {
                ActivityOptionsCompat.makeSceneTransitionAnimation(this@SignInActivity,
                        logoTV,
                        it)
            }
            startActivity(SignUpActivity.getStartIntent(this), options?.toBundle())
        }
    }

    override fun onSuccess(user: UserResponse) {
        val loginPref = LoginPref(this)
        loginPref.setAccessToken(baseContext, user.token!!)
        // Timber.d(user.toString())
        user.userSignUp?.let {
            loginPref.saveUser(it)
        }

        startActivity(CityFragment.getStartIntent(this))
//        FirebaseMessaging.getInstance().subscribeToTopic("user_${user.user?.id}")

        loginPref.getNotificationToken()?.let {
            signInPresenter.subscribeNotification(it)
        }
        finish()
    }

    override fun onDestroy() {
        signInPresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, SignInActivity::class.java)

        init {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }
    }
}