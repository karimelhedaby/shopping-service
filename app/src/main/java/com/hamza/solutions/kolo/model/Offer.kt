package com.hamza.solutions.kolo.model

import android.annotation.SuppressLint
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Offer(
        @SerializedName("imgs") var imgs: List<String>,
        @SerializedName("avaliable") var avaliable: Boolean,
        @SerializedName("_id") var id: String,
        @SerializedName("title") var title: String,
        @SerializedName("pricepreDisc") var pricepreDisc: String,
        @SerializedName("pricepostDisc") var pricepostDisc: String,
        @SerializedName("brandName") var brandName: String,
        @SerializedName("desc") var desc: String,
        @SerializedName("discound") var discound: String,
        @SerializedName("creationDate") var creationDate: String,
        @SerializedName("__v") var v: Int
): Parcelable