package com.hamza.rawaaprovider.ui.login

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.rawaa.hamza.rawaaprovider.service.responed.UserResponse


interface SignInContract {

    interface View : MvpView {
        fun onSuccess(user: UserResponse)
    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun signIn(phone: String?, password: String?)
        fun subscribeNotification(token: String)
    }
}