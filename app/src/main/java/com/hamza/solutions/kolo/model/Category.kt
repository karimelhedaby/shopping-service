package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Category(
        @SerializedName("image") val image: String,
        @SerializedName("_id") val id: String,
        @SerializedName("name") val name: String,
        @SerializedName("creationDate") val creationDate: String,
        @SerializedName("categoryIdentity") val categoryIdentity: String
):Parcelable