package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Shop

interface ShopsContract {

    interface View : MvpView {
        fun onShopsRecevied(shops :List<Shop>)
        fun onClothsShopTypeRecevied(shops: List<Shop>)
        fun onAdsResponse(ads : ArrayList<AdsResponse>)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getShops(categoryId : String , city:String)
        fun getClothsShopByType(categoryId: String, clothsType: String , city: String)
        fun getAds()

    }
}