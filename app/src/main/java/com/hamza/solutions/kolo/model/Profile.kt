package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class Profile(
        @SerializedName("poients") var poients: String,
        @SerializedName("type") var type: String,
        @SerializedName("creationDate") var creationDate: String,
        @SerializedName("_id") var id: String,
        @SerializedName("name") var name: String,
        @SerializedName("phone") var phone: String,
        @SerializedName("email") var email: String,
        @SerializedName("password") var password: String
)