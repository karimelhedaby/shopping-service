package com.hamza.solutions.kolo.ui.home

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.ProductsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Product
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class ProductsPresenter<V : ProductsContract.View> : BasePresenter<V>(), ProductsContract.Presenter<V> {


    override fun getProducts(id: String) {
        // Timber.d("test send")
        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

            mvpView?.showLoading()
            dataManager.getProducts(id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Product>>(mvpView!!) {
                        override fun onSuccess(t: List<Product>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onProductsRecived(t)
                        }

                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    override fun getSearchProducts(id: String, keyword: String) {
        // Timber.d("test send")
        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

//            mvpView?.showLoading()
            dataManager.searchProducts(id, keyword).subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Product>>(mvpView!!) {
                        override fun onSuccess(t: List<Product>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onSearchProductsRecieved(t)
                        }

                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    private var dataManager: DataManager = AppDataManager()
}