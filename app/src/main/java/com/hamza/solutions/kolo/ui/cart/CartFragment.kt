package com.hamza.rawaaprovider.ui.cart

import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.order.offer.OrderOfferFragment
import com.hamza.solutions.kolo.ui.order.products.OrderProductsFragment
import kotlinx.android.synthetic.main.fragment_cart.*
import java.util.*


class CartFragment : BaseFragment(), CartContract.View {

    private var cartAdapter: CartAdapter? = null
    private val fragments = ArrayList<BaseFragment>()

    override fun getFragmentView(): Int = R.layout.fragment_cart

    override fun setUp(view: View?) {
        if (fragments.isEmpty()) {
            context?.getString(R.string.title_orders)?.let { OrderProductsFragment.newInstance(it) }?.let { fragments.add(it) }
            context?.getString(R.string.title_offers)?.let { OrderOfferFragment.newInstance(it) }?.let { fragments.add(it) }
        }

        cartAdapter = CartAdapter(childFragmentManager, fragments, this.context!!)

        cartPager.adapter = cartAdapter
        cartTabLayout.setupWithViewPager(cartPager)
    }

    companion object {
        fun newInstance(): CartFragment = CartFragment()
    }
}