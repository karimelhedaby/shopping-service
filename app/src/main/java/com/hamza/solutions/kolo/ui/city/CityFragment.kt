package com.hamza.solutions.kolo.ui.city;

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.widget.Toast
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.R.id.cityRV
import com.hamza.solutions.kolo.model.City
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.city.adapter.CityAdapter
import com.hamza.solutions.kolo.ui.main.MainActivity
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_city.*

class CityFragment : BaseActivity(), CityContract.View, CityAdapter.cityListener {

    private lateinit var presenter: CityContract.Presenter<CityFragment>

    private lateinit var loginPref: LoginPref
//    override fun getFragmentView(): Int = R.layout.fragment_city

    override fun getActivityView(): Int = R.layout.fragment_city

    override fun afterInflation(savedInstance: Bundle?) {

        cityLogoIV.loadImage(R.drawable.kolologo)

        loginPref = this?.let { LoginPref(it) }!!
        var city = loginPref?.getCity()

        presenter = CityPresenter()
        presenter.onAttach(this)
        presenter.getCities()

        if (city?.name != null) {
            choosenCityTV.text = getString(R.string.choosen_city) + city!!.name
        }
        letsGOB.setOnClickListener {

            city = loginPref?.getCity()
            if (city?.name == null) {
                Toast.makeText(this.applicationContext, getString(R.string.please_choose_city), Toast.LENGTH_SHORT).show()
            } else {
                startActivity(Intent(this.applicationContext, MainActivity::class.java))
//            MainActivity.getStartIntent(this.applicationContext)
            }
        }
    }

    override fun onCitiesRecived(cities: List<City>) {
        cityRV.layoutManager = GridLayoutManager(this.applicationContext, 2)
        cityRV.addItemDecoration(
                GridSpacingItemDecoration(2, ViewUtils.dpToPx(8F), true, 0)
        )
        cityRV.adapter = this.applicationContext?.let { CityAdapter(it, cities, this) }
    }

    override fun onCityClickListener(city: City, position: Int) {
        val loginPref = LoginPref(this)
        loginPref.saveCity(city)
        choosenCityTV.setText("Choosen City : " + city.name)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, CityFragment::class.java)

        fun newInstance(): CityFragment {
            val fragment = CityFragment()
            return fragment
        }
    }
}
