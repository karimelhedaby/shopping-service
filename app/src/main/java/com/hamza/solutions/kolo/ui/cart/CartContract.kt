package com.hamza.rawaaprovider.ui.cart

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView


interface CartContract {

    interface View : MvpView

    interface Presenter<V : View> : MvpPresenter<V>
}