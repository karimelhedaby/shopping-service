package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class Test(
        @SerializedName("status") var status: String,
        @SerializedName("note") var note: String,
        @SerializedName("_id") var id: String,
        @SerializedName("user") var user: UserSignUp,
        @SerializedName("offers") var offers: Offer,
        @SerializedName("price") var price: String,
        @SerializedName("dliveryPrice") var dliveryPrice: String,
        @SerializedName("total") var total: String,
        @SerializedName("quantity") var quantity: String,
        @SerializedName("address") var address: String,
        @SerializedName("creationDate") var creationDate: String,
        @SerializedName("__v") var v: Int
)