package com.hamza.rawaa.ui.main

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView


interface MainContract {

    interface View : MvpView

    interface Presenter<V : View> : MvpPresenter<V>
}