package com.hamza.solutions.kolo.ui.home

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.ProductDetailsContract
import com.hamza.rawaa.ui.home.ProductsContract
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager


class ProductDetailsPresenter<V : ProductDetailsContract.View> : BasePresenter<V>(),ProductDetailsContract.Presenter<V>{


    private var dataManager: DataManager = AppDataManager()
}