package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class Delivery(
        @SerializedName("price") val price: Int,
        @SerializedName("_id") val id: String,
        @SerializedName("creationDate") val creationDate: String
)