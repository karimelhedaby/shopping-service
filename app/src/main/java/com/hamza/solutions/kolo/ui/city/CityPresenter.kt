package com.hamza.solutions.kolo.ui.city;


import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.City
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CityPresenter<V : CityContract.View> : BasePresenter<V>(), CityContract.Presenter<V> {

    override fun getCities() {
        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            dataManager.getCity()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<City>>(mvpView!!) {

                        override fun onSuccess(t: List<City>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onCitiesRecived(t)
                        }

                    }
                    )
        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    var dataManager: DataManager = AppDataManager()

}