package com.hamza.solutions.kolo.ui.home

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.OrderStepsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Delivery
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.OrderResponse
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OrderStepsPresenter<V : OrderStepsContract.View> : BasePresenter<V>(), OrderStepsContract.Presenter<V> {

    private var dataManager: DataManager = AppDataManager()


    override fun getDelivery() {


        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            dataManager.getDeliveryPrice()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<Delivery>(mvpView!!) {
                        override fun onSuccess(t: Delivery) {
                            // Timber.d("success send")
                            // Timber.d(t.toString())
                            mvpView?.hideLoading()
                            mvpView?.onDeliveryPriceRecived(t)
                        }
                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }


    }


    override fun sendOrder(order: Order) {

        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            dataManager.orderProducts(order)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<OrderResponse>(mvpView!!) {
                        override fun onSuccess(t: OrderResponse) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onOrderSend(t)
                        }
                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }


    }


}