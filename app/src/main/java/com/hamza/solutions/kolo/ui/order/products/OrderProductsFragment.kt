package com.hamza.solutions.kolo.ui.order.products

import android.support.v7.widget.GridLayoutManager
import android.view.View
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.CancleOrderResponse
import com.hamza.solutions.kolo.model.OrderOffer
import com.hamza.solutions.kolo.model.ProductsOrders
import com.hamza.solutions.kolo.model.UserSignUp
import com.hamza.solutions.kolo.ui.order.info.OrderOfferDetailsFragment
import com.hamza.solutions.kolo.ui.order.order.OrderDetailsFragment
import com.hamza.solutions.kolo.ui.order.products.adapter.OrderProductsAdapter
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_order_products.*


class OrderProductsFragment : BaseFragment(), OrderProductsContract.View {


    override fun getFragmentView(): Int = R.layout.fragment_order_products

    private lateinit var homePresenter: OrderProductsContract.Presenter<OrderProductsFragment>

    lateinit var user: UserSignUp

    override fun setUp(view: View?) {

        val loginPref = context?.let { LoginPref(it) }
        user = loginPref?.getUser()!!

        homePresenter = OrderProductsPresenter()
        homePresenter.onAttach(this)
        homePresenter.getProductsOrders(userId = user.id!!)

    }


    override fun onProductsOrdersRecived(productsOrders: List<ProductsOrders>) {
        // Timber.d(productsOrders.toString())

        if(productsOrders.isEmpty()){orderProductsEmptyTV.visibility=View.VISIBLE}

        orderProductsRV.layoutManager = GridLayoutManager(context, 1)
        orderProductsRV.addItemDecoration(
                GridSpacingItemDecoration(1, ViewUtils.dpToPx(8F), true, 0)
        )

        orderProductsRV.adapter = context?.let {
            OrderProductsAdapter(it, productsOrders, object : OrderProductsAdapter.orderClick {
                override fun onOrderClickListener(orderoffers: ProductsOrders) {
                    baseActivity?.replaceFragmentToActivity(OrderDetailsFragment.newInstance(orderoffers), true)
                }
            })
        }
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        fun newInstance(title: String): OrderProductsFragment {
            var orderProductFragment = OrderProductsFragment()
            orderProductFragment.title = title
            return orderProductFragment
        }
    }
}