package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Category
import com.hamza.solutions.kolo.service.retrofit.responed.CategoryResponse

interface HomeContract {

    interface View : MvpView {
        fun onCategoryRecived(categoryResponse: List<Category>)
        fun onAdsResponse(ads : ArrayList<AdsResponse>)
    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getCategory()
        fun getAds()
    }
}