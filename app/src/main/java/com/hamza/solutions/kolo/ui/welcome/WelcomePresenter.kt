package com.hamza.solutions.kolo.ui.welcome;

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager

class WelcomePresenter<V : WelcomeContract.View> : BasePresenter<V>(), WelcomeContract.Presenter<V> {

    var dataManager: DataManager = AppDataManager()
}