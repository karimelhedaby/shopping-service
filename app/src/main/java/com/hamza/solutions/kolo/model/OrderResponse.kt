package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class OrderResponse(
        @SerializedName("products") val products: List<String>,
        @SerializedName("quantity") val quantity: List<Int>,
        @SerializedName("status") val status: String,
        @SerializedName("_id") val id: String,
        @SerializedName("address") val address: String,
        @SerializedName("productPrice") val productPrice: Int,
        @SerializedName("user") val user: String,
        @SerializedName("deliveryPrice") val deliveryPrice: Int,
        @SerializedName("totalPrice") val totalPrice: Int,
        @SerializedName("notes") val notes: String,
        @SerializedName("creationDate") val creationDate: String
)