package com.hamza.solutions.kolo.ui.clothes.order.steps

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseFragment
import com.hamza.rawaa.ui.home.OrderStepsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Delivery
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.OrderResponse
import com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails.LocationDialog
import com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails.NotesDialog
import com.hamza.solutions.kolo.ui.home.OrderStepsPresenter
import kotlinx.android.synthetic.main.fragment_order_steps.*

class OrderStepsFragment : BaseFragment(), OrderStepsContract.View, NotesDialog.Companion.okClick, LocationDialog.Companion.okClickLoc {


    var locationOrder = ""
    var notesOrder = ""

    private var order = Order(notes = notesOrder, address = locationOrder, marketId = "")
    private var orderSend = Order(notes = notesOrder, address = locationOrder, marketId = "")

    override fun getFragmentView(): Int = R.layout.fragment_order_steps

    private lateinit var homePresenter: OrderStepsContract.Presenter<OrderStepsFragment>


    override fun onOkClickLoc(location: String) {
        locationOrder = location
        locationhET.text = locationOrder
    }

    override fun onOkClick(notes: String) {
        notesOrder = notes
        noteshET.text = notesOrder

    }


    override fun onDeliveryPriceRecived(delivery: Delivery) {
        priceET.text = " ${order.totalPrices} L.E + Delivry ${delivery.price} L.E = ${(order.totalPrices + delivery.price)}"
    }

    override fun setUp(view: View?) {

        homePresenter = OrderStepsPresenter()
        homePresenter.onAttach(this)
        homePresenter.getDelivery()

        order = arguments?.getParcelable(OrderStepsFragment.ARGUMENT_ORDER)!!
        order.marketId = arguments?.getString(OrderStepsFragment.MARKET_ID)!!
        setLocationBTN.setOnClickListener {
            val pop = LocationDialog(this)
            val fm = this@OrderStepsFragment.fragmentManager
            fm?.let {
                pop.show(it, "Address")
            }

        }

        setNotesBTN.setOnClickListener {

            val pop = NotesDialog(this)
            val fm = this@OrderStepsFragment.fragmentManager
            fm?.let { pop.show(it, "Notes") }
        }

        orderNowBTN.setOnClickListener {

            if (locationOrder.equals("")) {
                Toast.makeText(context, getString(R.string.you_must_enter_location), Toast.LENGTH_LONG).show()
            } else if (notesOrder.equals("")) {
                Toast.makeText(context, getString(R.string.you_must_enter_notes), Toast.LENGTH_LONG).show()

            } else {
                orderSend.marketId = order.marketId
                orderSend.totalPrices = order.totalPrices
                orderSend.products = order.products
                orderSend.address = locationOrder
                orderSend.notes = notesOrder
                // Timber.d(orderSend.toString())
                Log.d("+++++++", orderSend.toString())
                homePresenter.sendOrder(orderSend)
            }

        }

    }

    override fun onOrderSend(orderResponse: OrderResponse) {
        // Timber.d(orderResponse.toString() + "")
        orderNowBTN.visibility = View.GONE
        setLocationBTN.visibility = View.GONE
        setNotesBTN.visibility = View.GONE
        Toast.makeText(context, getString(R.string.succ_send_order), Toast.LENGTH_LONG).show()


    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }


    companion object {
        private val ARGUMENT_ORDER = "ARGUMENT_PRODUCT"
        private val MARKET_ID = "ARGUMENT_MARKET"

        fun newInstance(order: Order, shopId: String): OrderStepsFragment {
            val orderStepsFragment = OrderStepsFragment()
            val bundle = Bundle()
            bundle.putString(MARKET_ID, shopId)
            bundle.putParcelable(ARGUMENT_ORDER, order)
            orderStepsFragment.arguments = bundle
            return orderStepsFragment
        }
    }
}