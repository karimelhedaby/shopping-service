package com.hamza.solutions.kolo.ui.clothes

import com.hamza.solutions.kolo.model.Product

data class AddProduct(var product: Product)

data class RemoveProduct(var product: Product)