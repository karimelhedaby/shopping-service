package com.hamza.solutions.kolo.ui.order

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseDialog
import com.hamza.rawaaprovider.ui.register.OrderPresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Offer
import com.hamza.solutions.kolo.model.OrderOffer
import kotlinx.android.synthetic.main.dialog_order.*
import kotlinx.android.synthetic.main.dialog_order.view.*


class OrderDialog : BaseDialog(), OrderContract.View {

    private lateinit var orderPresenter: OrderContract.Presenter<OrderDialog>
    lateinit var offer: Offer


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_order, container, false)
        setUp(v)

        v.orderB.setOnClickListener {

            if (!quantityET.text?.isEmpty()!!) {
                showLoading()
                orderPresenter.sendOrder(offer.id, quantityET.text.toString().toInt(), addressET.text.toString())
            }
        }
        return v
    }


    override fun setUp(view: View?) {

        orderPresenter = OrderPresenter()
        orderPresenter.onAttach(this)
        offer = arguments?.getParcelable(ARGUMENT_OFFER)!!

    }

    override fun onOrderSend(orderOffer: OrderOffer) {
        hideLoading()
        Toast.makeText(context, "Successfuly send your orderOffer..", Toast.LENGTH_LONG).show()
        Toast.makeText(context, " " + orderOffer.toString(), Toast.LENGTH_LONG).show()
        dismissDialog("name")
    }

    companion object {
        private val ARGUMENT_OFFER = "ARGUMENT_OFFER"

        fun newInstance(offer: Offer): OrderDialog {
            val orderDialog = OrderDialog()
            val bundle = Bundle()
            bundle.putParcelable(ARGUMENT_OFFER, offer)
            orderDialog.arguments = bundle
            // Timber.d(offer.toString())
            orderDialog.showsDialog
            return orderDialog

        }
    }
}