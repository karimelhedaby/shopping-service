package com.hamza.solutions.kolo.ui.clothes.order.steps.moredetails

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.example.computec.breakfast.ui.base.BaseDialog
import com.hamza.solutions.kolo.R
import kotlinx.android.synthetic.main.dialog_details_order.view.*
import kotlinx.android.synthetic.main.dialog_notes_order.*
import kotlinx.android.synthetic.main.dialog_quantity_order.*

class Quantitydialog(var listener: okClick) : BaseDialog() {


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.dialog_quantity_order, container, false)
        setUp(v)

        v.okBTN.setOnClickListener {
            if (quantityET.text?.isEmpty()!!) {
                Toast.makeText(context, "quantity is Empty", Toast.LENGTH_LONG).show()
            } else {
                listener.onQuantityClick(quantityET.text.toString())
                dismiss()
            }

        }

        return v
    }
    override fun setUp(view: View?) {

    }

    companion object {

        fun newInstance(listener: okClick): NotesDialog {

            return newInstance(listener)
        }

        interface okClick {
            fun onQuantityClick(quantity: String)
        }
    }

}