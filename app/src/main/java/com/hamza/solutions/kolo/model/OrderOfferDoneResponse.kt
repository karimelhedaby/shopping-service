package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class OrderOfferDoneResponse(
        @SerializedName("status") val status: String,
        @SerializedName("note") val note: String,
        @SerializedName("_id") val id: String,
        @SerializedName("offers") val offers: String,
        @SerializedName("price") val price: String,
        @SerializedName("dliveryPrice") val dliveryPrice: String,
        @SerializedName("total") val total: String,
        @SerializedName("quantity") val quantity: String,
        @SerializedName("address") val address: String,
        @SerializedName("creationDate") val creationDate: String
)