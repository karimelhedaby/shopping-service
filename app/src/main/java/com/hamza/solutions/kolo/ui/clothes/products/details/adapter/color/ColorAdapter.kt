package com.hamza.solutions.kolo.ui.clothes.products.details.adapter.color

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Product
import kotlinx.android.synthetic.main.item_color.view.*

class ColorAdapter(var colors: MutableList<String>) : RecyclerView.Adapter<ColorVH>() {
    private var selectPosition = 0

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ColorVH {
        return ColorVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_color, null))
    }

    override fun onBindViewHolder(holder: ColorVH, position: Int) {
        with(holder.itemView) {
            try {
                colorBg.setBackgroundColor(Color.parseColor(colors[position]))
            } catch (e: Exception) {

            }
        }
    }

    fun getSelectedColor(): String {
        return colors[selectPosition]
    }

    fun setSelectedColor(position: Int) {
        selectPosition = position
    }


    open interface ProductClick {
        fun onProductClickListener(product: Product)
    }

    override fun getItemCount(): Int {
        return colors.size
    }
}


