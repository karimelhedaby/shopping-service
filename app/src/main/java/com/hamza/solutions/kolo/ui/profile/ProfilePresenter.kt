package com.hamza.rawaa.ui.profile

import android.util.Log
import com.example.computec.eltadreb.ui.base.BasePresenter
import com.google.gson.JsonObject
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Profile
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class ProfilePresenter<V : ProfileContract.View> : BasePresenter<V>(), ProfileContract.Presenter<V> {
    override fun getProfile() {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()

            dataManager.getProfile()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<Profile>(mvpView!!) {

                        override fun onSuccess(t: Profile) {
                            mvpView?.hideLoading()
                            mvpView?.onProfileRecevied(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }
                    })

        }
        else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }
    private var dataManager: DataManager = AppDataManager()

}