package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class OrderDoneResponse(
        @SerializedName("products") val products: List<String>,
        @SerializedName("quantity") val quantity: List<Int>,
        @SerializedName("status") val status: String,
        @SerializedName("_id") val id: String,
        @SerializedName("address") val address: String,
        @SerializedName("productPrice") val productPrice: Int
)