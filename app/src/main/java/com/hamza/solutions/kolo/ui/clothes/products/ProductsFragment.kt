package com.hamza.solutions.kolo.ui.clothes.products

import android.os.Build
import android.os.Bundle
import android.support.design.widget.CoordinatorLayout
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.Toolbar
import android.view.*
import android.widget.SearchView
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.loadImage
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaa.ui.home.ProductsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.Product
import com.hamza.solutions.kolo.model.Shop
import com.hamza.solutions.kolo.ui.clothes.AddProduct
import com.hamza.solutions.kolo.ui.clothes.RemoveProduct
import com.hamza.solutions.kolo.ui.clothes.order.steps.OrderStepsFragment
import com.hamza.solutions.kolo.ui.clothes.products.adapter.ProductAdapter
import com.hamza.solutions.kolo.ui.clothes.products.details.ProductDetailsFragment
import com.hamza.solutions.kolo.ui.home.ProductsPresenter
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import de.hdodenhof.circleimageview.CircleImageView
import kotlinx.android.synthetic.main.fragment_products.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class ProductsFragment : BaseFragment(), ProductsContract.View, ProductAdapter.ProductClick {

    private var orderMM: MutableMap<String, Product> = mutableMapOf()
    private lateinit var order: Order
    private var totalprice: Double = 0.0
    private lateinit var homePresenter: ProductsContract.Presenter<ProductsFragment>


    override fun getFragmentView(): Int = R.layout.fragment_products

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = super.onCreateView(inflater, container, savedInstanceState)

        var toolbar: Toolbar = view!!.findViewById(R.id.toolbar)
        toolbar.title = ""
        baseActivity?.setSupportActionBar(toolbar)

        setHasOptionsMenu(true)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val b = arguments
            val transitionName = b?.getString(TRANSITION_NAME)
            val logo: CircleImageView? = view?.findViewById<CircleImageView>(R.id.shopLogoIV)
            logo?.transitionName = transitionName
            // Timber.d(logo.toString() + " " + transitionName)

        }
        return view
    }

    override fun setUp(view: View?) {

        homePresenter = ProductsPresenter()
        homePresenter.onAttach(this)
        shopBgIV.loadImage(R.drawable.shop_bg)

        homePresenter.getProducts(shop.id)

        var address = ""
        var notes = ""
        order = Order(address = address, notes = notes, marketId = "")

        orderItemsFBTN.setOnClickListener {
            order.products = orderMM.values.toMutableList()

            // Timber.d(order.toString())
            if (orderMM.values.isNotEmpty()) {
                baseActivity?.replaceFragmentToActivity(OrderStepsFragment.newInstance(order, shop.id), true)
            } else {
                showMessage(getString(R.string.please_add_items))
            }

        }
    }

    override fun onProductsRecived(productResponse: List<Product>) {

        shopLogoIV.loadImage(shop.logo)
        shopBgIV.loadImage(shop.cover)
        shopHeadTV.text = shop.nameOfShop

        if (productResponse.isEmpty()) {
            productsEmptyTV.visibility = View.VISIBLE
        }

        productsRV.layoutManager = GridLayoutManager(context, 2)
        productsRV.addItemDecoration(
                GridSpacingItemDecoration(2, ViewUtils.dpToPx(8F), true, 0)
        )

        productsRV.adapter = ProductAdapter(productResponse, this)
    }

    override fun onSearchProductsRecieved(productResponse: List<Product>) {

        productsRV.layoutManager = GridLayoutManager(context, 2)
        productsRV.adapter = ProductAdapter(productResponse, this)

    }

    override fun onProductClickListener(product: Product) {

        // Timber.d(product.toString() + " PPPP")
        baseActivity?.startActivity(ProductDetailsFragment.getStartIntent(baseActivity!!, product))
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onAddOrder(addProduct: AddProduct) {
        with(addProduct.product) {
            orderMM[id] = this
        }
        // Timber.d("add ${orderMM.values.toMutableList()}")
        // Timber.d(orderMM.toString())
        calculatePrice()
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRemoveOrder(removeProduct: RemoveProduct) {
        // Timber.d("remove")
        with(removeProduct.product) {
            orderMM.remove(id)
        }

        // Timber.d(orderMM.toString())

        calculatePrice()
    }

    private fun calculatePrice() {
        // Timber.d(orderMM.values.toMutableList().size.toString())
        // Timber.d(totalprice.toString())
        order.totalPrices = orderMM.entries.sumByDouble { (_, value) ->
            value.price.toDouble().times(value.quantity)
        }
        // Timber.d(order.totalPrices.toString())
    }

    override fun onStop() {
        EventBus.getDefault().unregister(this)
        super.onStop()
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    companion object {
        private lateinit var shop: Shop
        private val TRANSITION_NAME = "TRANSITION_NAME"


        fun newInstance(transtionName: String, currentshop: Shop): ProductsFragment {

            val productFragment = ProductsFragment()
            val bundle = Bundle()
            bundle.putString(TRANSITION_NAME, transtionName)
            productFragment.arguments = bundle
            shop = currentshop
            return productFragment
        }
    }

    private var searchMenuItem: MenuItem? = null
    private var searchView: SearchView? = null

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.inflate(R.menu.products_menu, menu)


        val mSearch = menu?.findItem(R.id.searchMenu)

        val mSearchView = mSearch?.actionView as SearchView
        mSearchView.queryHint = "Search"

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String): Boolean {
                homePresenter.getSearchProducts(shop.id, query)
                return false
            }

            override fun onQueryTextChange(newText: String): Boolean {
                homePresenter.getSearchProducts(shop.id, newText)
                return true
            }
        })

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.searchMenu -> {
                val params = appbar.getLayoutParams() as CoordinatorLayout.LayoutParams
                params.height = 3 * 80 // HEIGHT

                appbar.setLayoutParams(params)
                appbar.setExpanded(false)
                true
            }
            else -> super.onOptionsItemSelected(item)

        }
    }
//    fun  collapseToolbar(){
//        val params = appbar.getLayoutParams() as CoordinatorLayout.LayoutParams
//        behavior = params.behavior as AppBarLayout.Behavior
//        if(behavior!=null) {
//            behavior.onNestedFling(rootLayout, appbarLayout, null, 0, 10000, true)
//        }
}

//        mSearchView.setOnCloseListener {
//
//        }
//
//        super.onCreateOptionsMenu(menu, inflater)
//
//    }
//
//    fun collapseToolbar() : Unit {
//        val params = appbar.getLayoutParams() as CoordinatorLayout.LayoutParams
//       var behavior = params.behavior as AppBarLayout.Behavior?
//        if (behavior != null) {
//            behavior.onNestedFling(coordinate, appbar, collapsing, 0 as  Float, 10000 as Float, true)
//        }
//    }

