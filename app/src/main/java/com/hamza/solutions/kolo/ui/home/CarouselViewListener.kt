package com.hamza.solutions.kolo.ui.home

import android.app.Activity
import android.view.View
import android.widget.ImageView
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.AdsResponse
import com.synnapps.carouselview.ViewListener

class CarouselViewListener(var activity: Activity, var ads: ArrayList<AdsResponse>) : ViewListener {


    override fun setViewForPosition(position: Int): View {
        val customView = activity.layoutInflater.inflate(R.layout.item_ad_carousel_view, null)
        with(customView) {
            this.findViewById<ImageView>(R.id.adIV).loadImage(ads[position].image)
        }
        return customView
    }
}