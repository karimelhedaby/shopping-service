package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class AdsResponse(
        @SerializedName("image") var image: String,
        @SerializedName("avaliable") var avaliable: String,
        @SerializedName("_id") var id: String,
        @SerializedName("text") var text: String,
        @SerializedName("creationDate") var creationDate: String
)