package com.hamza.solutions.kolo.model

import com.google.gson.annotations.SerializedName

data class City(
        @SerializedName("image") var image: String,
        @SerializedName("_id") var id: String,
        @SerializedName("name") var name: String,
        @SerializedName("creationDate") var creationDate: String,
        @SerializedName("__v") var v: Int
)