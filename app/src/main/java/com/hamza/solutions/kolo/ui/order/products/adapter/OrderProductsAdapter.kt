package com.hamza.solutions.kolo.ui.order.products.adapter

import android.content.Context
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.text.format.DateUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.computec.eltadreb.utils.loadImage
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.ProductsOrders
import kotlinx.android.synthetic.main.item_cart_order.view.*

import java.util.*

/**
 * Created by karim on 7/18/18.
 */
class OrderProductsAdapter(var context: Context, var orders: List<ProductsOrders>, var orderListener: OrderProductsAdapter.orderClick) : RecyclerView.Adapter<OrderProductsAdapter.OfferVH>() {


    class OfferVH(itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferVH {
        return OfferVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.item_order_product, null))
    }

    override fun onBindViewHolder(holder: OfferVH, position: Int) {
        with(holder.itemView) {
            orders[position]
                    .productDetails
                    .find { it.product.imgs.isNotEmpty() }
                    ?.let {
                        productIV.loadImage(it.product.imgs[0])
                    }

            // Timber.d(orders[position].productDetails.toString())
            priceTV.text = orders[position].totalPrice.toString() + " L.E"

            setOnClickListener {
                orderListener.onOrderClickListener(orders[position])
                // Timber.d(orders[position].creationDate)
            }

            // Timber.d(orders[position].creationDate)
            val date = DateUtils
                    .getRelativeTimeSpanString(com.rawaa.hamza.rawaa.utils.DateUtils.convertUTCZToMills(orders[position].creationDate),
                            Calendar.getInstance().timeInMillis, DateUtils.MINUTE_IN_MILLIS) as String
            dateTV.text = date

            val statusIcon = when (orders[position].status) {
                "OnTheWay" -> R.drawable.ic_shopping_delivering
                "Done" -> R.drawable.ic_delivered
                "Accepted" -> R.drawable.ic_verify
                "Pindding" -> R.drawable.ic_cart
                "Rejected" -> R.drawable.ic_refuse
                "Cancle" -> R.drawable.ic_cancle
                else -> 0
            }

            statusIV.setImageDrawable((ContextCompat.getDrawable(context, statusIcon)))
        }
    }

    open interface orderClick {
        fun onOrderClickListener(orderoffers: ProductsOrders)
    }

    override fun getItemCount(): Int {
        return orders.size
    }
}