package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Shop

interface CategoriesContract {

    interface View : MvpView {


    }

    interface Presenter<V : View> : MvpPresenter<V> {


    }
}