package com.rawaa.hamza.rawaa.ui.cart.order.details

import android.util.Log
import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.CancleOfferOrderResponse
import com.hamza.solutions.kolo.model.CancleOrderResponse
import com.hamza.solutions.kolo.model.OrderDoneResponse
import com.hamza.solutions.kolo.model.OrderOfferDoneResponse
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class OrderDetailsPresenter<V : OrderDetailsContract.View> : BasePresenter<V>(), OrderDetailsContract.Presenter<V> {
    override fun cancleOrder(orderId: String) {

        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

            mvpView?.showLoading()
            dataManager.cancleOrder(orderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<CancleOrderResponse>(mvpView!!) {

                        override fun onSuccess(t: CancleOrderResponse) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.oncancleOrder(t)
                        }

                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    override fun orderOfferCancle(orderId: String) {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.cancleOfferOrder(orderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<CancleOfferOrderResponse>(mvpView!!) {
                        override fun onSuccess(t: CancleOfferOrderResponse) {
                            mvpView?.hideLoading()
                            mvpView?.onOrderOfferCancled(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }
                    })

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    override fun orderOfferDone(offerId: String) {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.sendOrderOfferDone(offerId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<OrderOfferDoneResponse>(mvpView!!) {
                        override fun onSuccess(t: OrderOfferDoneResponse) {
                            mvpView?.hideLoading()
                            mvpView?.onOrderOfferDone(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }
                    })

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    override fun orderDone(orderId: String) {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.sendOrderDone(orderId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<OrderDoneResponse>(mvpView!!) {
                        override fun onSuccess(t: OrderDoneResponse) {
                            mvpView?.hideLoading()
                            mvpView?.onOrderDone(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }
                    })

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    private var dataManager: DataManager = AppDataManager()
}