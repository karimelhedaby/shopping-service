package com.hamza.solutions.kolo.ui.order
import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.OrderOffer

interface OrderContract {
    interface View : MvpView {
        fun onOrderSend(orderOffer: OrderOffer)
    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun sendOrder(offerId: String, quantity: Int, address: String)
    }
}