package com.rawaa.hamza.rawaa.service.firebase

interface BaseLisener<T, E> {

    fun onSuccess(data: T)

    fun onFail(error: E)
}
