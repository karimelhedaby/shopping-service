package com.hamza.solutions.kolo.ui.home

import android.util.Log
import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.rawaa.ui.home.ShopsContract
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.AdsResponse
import com.hamza.solutions.kolo.model.Shop
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers



class ShopsPresenter<V : ShopsContract.View> : BasePresenter<V>(), ShopsContract.Presenter<V> {


    private var dataManager: DataManager = AppDataManager()

    override fun getShops(categoryId: String, city: String) {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.getShops(categoryId, city)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Shop>>(mvpView!!) {
                        override fun onSuccess(t: List<Shop>) {
                            mvpView?.hideLoading()
                            mvpView?.onShopsRecevied(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }

                    }
                    )
        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    override fun getClothsShopByType(categoryId: String, clothsType: String , city :String) {

        if (mvpView!!.isNetworkConnected()) {

            mvpView?.showLoading()
            dataManager.getClothsShopsByType(categoryId, clothsType,city)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<Shop>>(mvpView!!) {
                        override fun onSuccess(t: List<Shop>) {
                            mvpView?.hideLoading()
                            mvpView?.onClothsShopTypeRecevied(t)
                            Log.d("++++++++", t.toString())
                        }

                        override fun onError(e: Throwable) {
                            super.onError(e)
                            // Timber.d(e.message.toString())
                        }

                    }
                    )
        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }


    }

    override fun getAds() {
        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            dataManager.getAds()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<ArrayList<AdsResponse>>(mvpView!!) {
                        override fun onSuccess(t: ArrayList<AdsResponse>) {
                            mvpView?.hideLoading()
                            mvpView?.onAdsResponse(t)
                        }
                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }


}