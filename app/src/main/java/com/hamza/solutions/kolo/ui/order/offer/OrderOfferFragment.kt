package com.hamza.solutions.kolo.ui.order.offer

import android.support.v7.widget.GridLayoutManager
import android.view.View
import com.example.computec.breakfast.ui.base.BaseFragment
import com.example.computec.eltadreb.utils.ViewUtils
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.OrderOffer
import com.hamza.solutions.kolo.model.UserSignUp
import com.hamza.solutions.kolo.ui.order.offer.adapter.OrderOfferAdapter
import com.hamza.solutions.kolo.utils.GridSpacingItemDecoration
import com.hamza.solutions.kolo.ui.order.info.OrderOfferDetailsFragment
import com.rawaa.hamza.rawaaprovider.service.retrofit.LoginPref
import kotlinx.android.synthetic.main.fragment_order_offers.*


class OrderOfferFragment : BaseFragment(), OrderOffersContract.View, OrderOfferAdapter.orderClick {

    override fun getFragmentView(): Int = R.layout.fragment_order_offers

    private lateinit var homePresenter: OrderOffersContract.Presenter<OrderOfferFragment>

    lateinit var user: UserSignUp

    override fun setUp(view: View?) {


        homePresenter = OrderOfferPresenter()
        homePresenter.onAttach(this)

        val loginPref = context?.let { LoginPref(it) }
        user = loginPref?.getUser()!!

        homePresenter.getOrderOffers(user.id.toString())
    }


    override fun onOrderOfferRecived(offerResponce: List<OrderOffer>) {
        // Timber.d(offerResponce.toString())

        if(offerResponce.isEmpty()){orderEmptyTV.visibility=View.VISIBLE}

        orderofferRV.layoutManager = GridLayoutManager(context, 1)
        orderofferRV.addItemDecoration(
                GridSpacingItemDecoration(1, ViewUtils.dpToPx(8F), true, 0)
        )
        orderofferRV.adapter = context?.let { OrderOfferAdapter(it, offerResponce, this) }
    }

    override fun onDestroy() {
        homePresenter.onDetach()
        super.onDestroy()
    }

    override fun onOrderClickListener(orderoffers: OrderOffer) {
        baseActivity?.replaceFragmentToActivity(OrderOfferDetailsFragment.newInstance(orderoffers), true)
    }

    companion object {
        fun newInstance(title: String): OrderOfferFragment {
            var orderProductFragment = OrderOfferFragment()
            orderProductFragment.title = title
            return orderProductFragment
        }
    }
}