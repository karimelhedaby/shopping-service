package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize


@Parcelize
data class UserSignUp(
        @field:SerializedName("img")
        val img: String? = null,

        @field:SerializedName("email")
        val email: String? = null,

        @field:SerializedName("phone")
        val phone: String? = null,

        @field:SerializedName("name")
        val name: String? = null,

        @field:SerializedName("password")
        val password: String? = null,

        @field:SerializedName("_id")
        val id: String? = null
): Parcelable

data class ChangePassword(
        var email: String? = null,
        var code: String? = null,
        var newPassword: String? = null
)