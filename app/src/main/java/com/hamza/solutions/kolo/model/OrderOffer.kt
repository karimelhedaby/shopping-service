package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class OrderOffer(
        @SerializedName("status") var status: String,
        @SerializedName("_id" ,alternate = ["id"]) var id: String,

        @SerializedName("price") var price: String,
        @SerializedName("dliveryPrice") var dliveryPrice: String,
        @SerializedName("total") var total: String,

        @SerializedName("user") var user: UserSignUp,
        @SerializedName("offers") var offers: Offer,

        @SerializedName("quantity") var quantity: String,
        @SerializedName("address") var address: String,
        @SerializedName("creationDate") var creationDate: String,
        @SerializedName("__v") var v: Int
) : Parcelable

@Parcelize
data class Order(
        @SerializedName("products") var products: List<Product> = listOf(),
        @SerializedName("productPrice") var totalPrices: Double = 0.0,
        @SerializedName("address") var address: String,
        @SerializedName("notes") var notes: String,
        @SerializedName("market") var marketId: String
) : Parcelable