package com.hamza.solutions.kolo.ui.offers.adapter

import android.app.Activity
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Offer
import com.hamza.solutions.kolo.ui.offers.CarouselViewListener
import kotlinx.android.synthetic.main.offer_item.view.*

/**
 * Created by karim on 7/18/18.
 */
class OfferAdapter(var context: Context, var offers: List<Offer>, var offerOrderListener: offerOrderClick) : RecyclerView.Adapter<OfferAdapter.OfferVH>() {


    class OfferVH(itemView: View) : RecyclerView.ViewHolder(itemView) {}


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OfferVH {
        return OfferVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.offer_item, null))
    }

    override fun onBindViewHolder(holder: OfferVH, position: Int) {


        with(holder.itemView) {

            offerCV.setViewListener(this.let { CarouselViewListener(context as Activity, offers[position].imgs) })
            offerCV.pageCount = offers[position].imgs.size

            offer_titleTV.text = offers[position].title
            offer_brandTV.text = offers[position].brandName
            offer_discriptionTV.text = offers[position].desc
            offer_postdisTV.text = "${offers[position].pricepostDisc} L.E"
            offer_predisTV.text = "${offers[position].pricepreDisc} L.E"
            offer_discTV.text = offers[position].discound

            offer_orderB.setOnClickListener({
                offerOrderListener.onOfferOrderClickListener(offers[position])
            })
        }
    }

    override fun getItemCount(): Int {
        return offers.size
    }

    open interface offerOrderClick {
        fun onOfferOrderClickListener(offer: Offer)
    }
}