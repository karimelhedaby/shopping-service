package com.rawaa.hamza.rawaa.ui.cart.order.details

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.OrderDoneResponse
import com.hamza.solutions.kolo.model.OrderOfferDoneResponse
import com.hamza.solutions.kolo.model.CancleOfferOrderResponse
import com.hamza.solutions.kolo.model.CancleOrderResponse


interface OrderDetailsContract {

    interface View : MvpView {
        fun onOrderDone(doneOrder: OrderDoneResponse)
        fun onOrderOfferDone(offerDone: OrderOfferDoneResponse)
        fun onOrderOfferCancled(cancleOfferOrderResponse: CancleOfferOrderResponse)

        fun oncancleOrder(cancleOrderResponse: CancleOrderResponse)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun orderDone(orderId: String)
        fun orderOfferDone(offerId: String)
        fun orderOfferCancle(orderId: String)

        fun cancleOrder(orderId: String)

    }
}