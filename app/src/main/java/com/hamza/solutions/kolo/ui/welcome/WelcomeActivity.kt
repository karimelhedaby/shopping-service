package com.hamza.solutions.kolo.ui.welcome;

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.widget.ImageView
import android.widget.TextView
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.login.SignInActivity
import kotlinx.android.synthetic.main.activity_welcome.*


class WelcomeActivity : BaseActivity(), WelcomeContract.View {

    private lateinit var presenter: WelcomeContract.Presenter<WelcomeActivity>

    private var myViewPagerAdapter = MyViewPagerAdapter()
    private lateinit var dots: ArrayList<TextView>
    private var layouts: ArrayList<Int> = arrayListOf()


    override fun getActivityView(): Int = R.layout.activity_welcome

    override fun afterInflation(savedInstance: Bundle?) {
        presenter = WelcomePresenter()
        presenter.onAttach(this)


        if (Build.VERSION.SDK_INT >= 21) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or
                    View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        }

        layouts.add(R.layout.welcome_slide1)
        layouts.add(R.layout.welcome_slide2)
        layouts.add(R.layout.welcome_slide3)

        //mMediaPlayer.start()

        dots = arrayListOf()
        dots.add(TextView(this))
        dots.add(TextView(this))
        dots.add(TextView(this))
        addBottomDots(0)
        changeStatusBarColor()

        view_pager?.adapter = myViewPagerAdapter
        view_pager?.addOnPageChangeListener(viewPagerPageChangeListener)


        btn_next.setOnClickListener {
            // checking for last page
            // if last page home screen will be launched
            val current = getItem(+1)
            if (current < layouts.size) {
                // move to next screen
                view_pager?.currentItem = current
            } else {
                startActivity(SignInActivity.getStartIntent(this))
                this.finish()
            }
        }

        btn_skip.setOnClickListener {
            startActivity(SignInActivity.getStartIntent(this))
            this.finish()
        }
    }

    private fun addBottomDots(currentPage: Int) {


        //Toast.makeText(this,currentPage.toString(),Toast.LENGTH_LONG).show()

        val colorsActive: ArrayList<Int> = arrayListOf(
                resources.getIntArray(R.array.array_dot_active)[0],
                resources.getIntArray(R.array.array_dot_active)[1],
                resources.getIntArray(R.array.array_dot_active)[2]
        )
        val colorsInactive: ArrayList<Int> = arrayListOf(
                resources.getIntArray(R.array.array_dot_inactive)[0],
                resources.getIntArray(R.array.array_dot_inactive)[1],
                resources.getIntArray(R.array.array_dot_inactive)[2]
        )

        layoutDots?.removeAllViews()

        for (i in 0..dots.size - 1) {
            dots[i].text = Html.fromHtml("&#8226;")
            dots[i].textSize = 35F
            dots[i].setTextColor(colorsInactive[currentPage])
            layoutDots?.addView(dots[i])
        }

        if (dots.size > 0)
            dots[currentPage].setTextColor(colorsActive[currentPage])
    }

    private fun getItem(i: Int): Int {
        return view_pager?.currentItem!! + i
    }

    //  viewpager change listener
    var viewPagerPageChangeListener: ViewPager.OnPageChangeListener = object : ViewPager.OnPageChangeListener {

        override fun onPageSelected(position: Int) {
            addBottomDots(position)

            // changing the next button text 'NEXT' / 'GOT IT'
            if (position == layouts.size - 1) {
                // last page. make button text to GOT IT
                btn_next.setText(getString(R.string.start))
                btn_skip.setVisibility(View.GONE)
            } else if (position == 0) {

                //mMediaPlayer.start()
            } else {
                // still pages are left
                btn_next.setText(getString(R.string.next))
                btn_skip.setVisibility(View.VISIBLE)
            }
        }

        override fun onPageScrolled(arg0: Int, arg1: Float, arg2: Int) {

        }

        override fun onPageScrollStateChanged(arg0: Int) {

        }
    }

    private fun changeStatusBarColor() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val window = window
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = Color.TRANSPARENT
        }
    }


    inner class MyViewPagerAdapter : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null

        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater = getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            val view = layoutInflater!!.inflate(layouts[position], container, false)
            container.addView(view)
            /*when (position) {
                0 -> view.findViewById<ImageView>(R.id.backgroundIV1).loadImage(R.drawable.image_welcome_first)
                1 -> view.findViewById<ImageView>(R.id.backgroundIV2).loadImage(R.drawable.image_welcome_second)
                2 -> view.findViewById<ImageView>(R.id.backgroundIV3).loadImage(R.drawable.image_welcome_third)
            }*/

            return view
        }

        override fun getCount(): Int {
            return layouts.size
        }

        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }

    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, WelcomeActivity::class.java)
    }

    override fun onDestroy() {
        presenter.onDetach()
        super.onDestroy()
    }
}