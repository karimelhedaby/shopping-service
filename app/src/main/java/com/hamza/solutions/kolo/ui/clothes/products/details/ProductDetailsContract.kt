package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView

interface
ProductDetailsContract {

    interface View : MvpView {

    }

    interface Presenter<V : View> : MvpPresenter<V> {

    }
}