package com.hamza.rawaa.ui.home

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Product

interface
ProductsContract {

    interface View : MvpView {
        fun onProductsRecived(productResponse: List<Product>)
        fun onSearchProductsRecieved(productResponse: List<Product>)


    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getProducts(id: String)
        fun getSearchProducts(id: String,keyword:String)


    }
}