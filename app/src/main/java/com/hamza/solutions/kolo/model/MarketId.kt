package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MarketId(
        @SerializedName("logo") val logo: String,
        @SerializedName("cover") val cover: String,
        @SerializedName("_id") val id: String,
        @SerializedName("nameOfShop") val name: String,
        @SerializedName("categoryId") val categoryId: String,
        //@SerializedName("genderTarget") val genderTarget: String,
        @SerializedName("creationDate") val creationDate: String,
        @SerializedName("__v") val v: Int
) : Parcelable