package com.hamza.rawaaprovider.ui.cart

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.example.computec.breakfast.ui.base.BaseFragment
import java.util.*

/**
 * Created by Mohamed Fakhry on 30/03/2018.
 */
class CartAdapter(fm: FragmentManager, var fragments: ArrayList<BaseFragment>, var context: Context) : FragmentStatePagerAdapter(fm) {


    override fun getItem(position: Int): Fragment {
        return fragments[position]
    }

    fun addPage(fragment: BaseFragment) {
        fragments.add(fragment)
        notifyDataSetChanged()
    }

    fun replacePage(replaceFragment: BaseFragment, position: Int) {
        fragments[position] = replaceFragment
        notifyDataSetChanged()
    }

    fun removePage(index: Int) {
        fragments.removeAt(index)
        notifyDataSetChanged()
    }

    override fun getCount(): Int {
        return fragments.size
    }

    override fun getPageTitle(position: Int):
            CharSequence = fragments[position].title


//    override fun getItemPosition(`object`: Any?): Int {
//        return PagerAdapter.POSITION_NONE
//    }
}