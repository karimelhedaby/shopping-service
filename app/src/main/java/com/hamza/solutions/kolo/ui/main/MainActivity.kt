package com.hamza.solutions.kolo.ui.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.content.ContextCompat
import com.aurelhubert.ahbottomnavigation.AHBottomNavigation
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationAdapter
import com.example.computec.eltadreb.utils.replaceFragmentToActivity
import com.hamza.rawaaprovider.ui.cart.CartFragment
import com.hamza.solutions.kolo.ui.home.HomeFragment
import com.hamza.rawaaprovider.ui.profile.ProfileFragment
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.ui.base.BaseActivity
import com.hamza.solutions.kolo.ui.clothes.order.steps.OrderStepsFragment
import com.hamza.solutions.kolo.ui.offers.OfferFragment
import com.hamza.solutions.kolo.ui.order.offer.OrderOfferFragment
import com.hamza.solutions.kolo.ui.order.products.OrderProductsFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : BaseActivity(), AHBottomNavigation.OnTabSelectedListener {

    private var mainPageFragments: MutableList<Fragment> = mutableListOf()

    override fun getActivityView(): Int = R.layout.activity_main

    override fun afterInflation(savedInstance: Bundle?) {
        setupViewPager()
    }

    private fun setupViewPager() {
        mainPageFragments.add(HomeFragment.newInstance())
        mainPageFragments.add(OfferFragment.newInstance())
        mainPageFragments.add(ProfileFragment.newInstance())
        mainPageFragments.add(CartFragment.newInstance())


        replaceFragmentToActivity(mainPageFragments[0])

        val navigationAdapter = AHBottomNavigationAdapter(this, R.menu.bottom_navigation_menu)
        bottomNavigation.isTranslucentNavigationEnabled = false
        navigationAdapter.setupWithBottomNavigation(bottomNavigation)
        bottomNavigation.accentColor = ContextCompat.getColor(this, R.color.colorAccent)
        bottomNavigation.setOnTabSelectedListener(this)
    }

    override fun onTabSelected(position: Int, wasSelected: Boolean): Boolean {
        supportFragmentManager?.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
        if (position == mainPageFragments.size - 1)
            replaceFragmentToActivity(CartFragment.newInstance())
        else
            replaceFragmentToActivity(mainPageFragments[position])
        return true
    }

    companion object {
        fun getStartIntent(context: Context): Intent = Intent(context, MainActivity::class.java)
    }
}