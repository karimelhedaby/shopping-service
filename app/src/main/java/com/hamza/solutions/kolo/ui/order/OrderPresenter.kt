package com.hamza.rawaaprovider.ui.register

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.OrderOffer
import com.hamza.solutions.kolo.model.OrderOfferRequest
import com.hamza.solutions.kolo.ui.order.OrderContract
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


/**
 * Created by abdallah on 7/21/18.
 */

class OrderPresenter<V : OrderContract.View> : BasePresenter<V>(), OrderContract.Presenter<V> {

    var dataManager: DataManager = AppDataManager()

    override fun sendOrder(offerId: String, quantity: Int, address: String) {

        if (checkOrderData(quantity, address)) return

        if (mvpView!!.isNetworkConnected()) {
            mvpView?.showLoading()
            val orderRequest = OrderOfferRequest(offerId, quantity, address)

            dataManager.orderOffer(orderRequest)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<OrderOffer>(mvpView!!) {
                        override fun onSuccess(orderOffer: OrderOffer) {
                            if (isViewAttached()) {
                                // Timber.d("sennnnnnnt" + "")
                                mvpView?.hideLoading()
                                mvpView?.onOrderSend(orderOffer)
                                mvpView?.showMessage(R.string.order_send_msg)
                            }
                        }
                    })

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }

    }

    private fun checkOrderData(quantity: Int, address: String): Boolean {
        if (quantity <= 0) {
            mvpView?.onError(R.string.no_quantity_error)
            return true
        } else if (address.isEmpty()) {
            mvpView?.onError(R.string.no_adress_error)
            return true
        }
        return false
    }
}