package com.hamza.solutions.kolo.ui.order.offer

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.OrderOffer

interface
OrderOffersContract {

    interface View : MvpView {
        fun onOrderOfferRecived(offerResponce: List<OrderOffer>)


    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getOrderOffers(id: String)

    }
}