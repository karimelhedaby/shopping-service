package com.hamza.rawaaprovider.ui.cart

import com.example.computec.eltadreb.ui.base.BasePresenter


class CartPresenter<V : CartContract.View> : BasePresenter<V>(), CartContract.Presenter<V>