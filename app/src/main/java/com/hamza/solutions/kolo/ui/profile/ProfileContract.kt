package com.hamza.rawaa.ui.profile

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.Profile


interface ProfileContract {

    interface View : MvpView {
        fun onProfileRecevied(profile: Profile)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getProfile()
    }


}
