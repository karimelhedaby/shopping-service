package com.rawaa.hamza.rawaaprovider.service.retrofit

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson
import com.hamza.solutions.kolo.App
import com.hamza.solutions.kolo.model.City
import com.hamza.solutions.kolo.model.UserSignUp

class LoginPref(context: Context) {

    private var prefs: SharedPreferences? = null
    var accessToken: String? = null

    init {
        try {
            prefs = context.applicationContext.getSharedPreferences(LOGIN_PREF, Context.MODE_PRIVATE)
            accessToken = prefs?.getString(KEY_ACCESS_TOKEN, null)
        } catch (e: Exception) {
        }
    }

    fun setAccessToken(context: Context, accessToken: String) {
        try {
            if (!TextUtils.isEmpty(accessToken)) {
                this.accessToken = accessToken
                prefs!!.edit().putString(KEY_ACCESS_TOKEN, accessToken).apply()
                setSecureConnection()
            }

        } catch (e: Exception) {
        }
    }

    fun saveUser(userSignUp: UserSignUp) {
        val gson = Gson()
        val json = gson.toJson(userSignUp)
        prefs?.edit()?.putString("userSignUp", json)?.apply()
    }

    fun getUser(): UserSignUp? {
        val gson = Gson()
        val json = prefs!!.getString("userSignUp", null)
        return gson.fromJson(json, UserSignUp::class.java)
    }


    fun saveCity(city: City) {
        val gson = Gson()
        val json = gson.toJson(city)
        prefs?.edit()?.putString("city", json)?.apply()
    }

    fun getCity(): City? {
        val gson = Gson()
        val json = prefs!!.getString("city", null)
        return gson.fromJson(json, City::class.java)
    }

    fun saveLang(lang: String) {
        prefs?.edit()?.putString("lang", lang)?.apply()
    }

    fun getLang(): String {
        return prefs?.getString("lang", "en")!!
    }


    fun saveNotificationToken(token: String?) {
        if (!TextUtils.isEmpty(token)) {
            prefs?.edit()?.putString(KEY_NOTIFICATION_ACCESS_TOKEN, token)?.apply()
        }
    }

    fun logout() {
        val notificationToken = getNotificationToken()
        prefs?.edit()?.clear()?.apply()
        saveNotificationToken(notificationToken)
    }

    fun getNotificationToken(): String? {
//        Log.d("Test", prefs?.getString(KEY_NOTIFICATION_ACCESS_TOKEN, null))
        return prefs?.getString(KEY_NOTIFICATION_ACCESS_TOKEN, null)
    }

    fun removeAccessToken(context: Context) {
        try {
            this.accessToken = null
            prefs?.edit()?.putString(KEY_ACCESS_TOKEN, null)?.apply()
            removeSecureConnection()
        } catch (e: Exception) {
        }
    }

    fun setSecureConnection() {
        try {
            App.createApi(AuthInterceptor(this.accessToken))
        } catch (e: Exception) {
        }
    }

    fun removeSecureConnection() {
        App.createApi(null)
    }

    companion object {
        private val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
        private val LOGIN_PREF = "LOGIN_PREF"
        private val KEY_NOTIFICATION_ACCESS_TOKEN = "KEY_NOTIFICATION_ACCESS_TOKEN"
    }
}
