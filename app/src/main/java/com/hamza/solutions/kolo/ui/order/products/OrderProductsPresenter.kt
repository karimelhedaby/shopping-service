package com.hamza.solutions.kolo.ui.order.products

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.CancleOrderResponse
import com.hamza.solutions.kolo.model.ProductsOrders
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OrderProductsPresenter<V : OrderProductsContract.View> : BasePresenter<V>(), OrderProductsContract.Presenter<V> {


    private var dataManager: DataManager = AppDataManager()

    override fun getProductsOrders(userId: String) {
        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

            mvpView?.showLoading()
            dataManager.getProductOrders(userId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<ProductsOrders>>(mvpView!!) {

                        override fun onSuccess(t: List<ProductsOrders>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onProductsOrdersRecived(t)
                        }

                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }


}