package com.hamza.solutions.kolo.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductsOrders(
        @SerializedName("productDetails") val productDetails: List<ProductQuantity>,
        @SerializedName("status") val status: String,
        @SerializedName("_id") val id: String,
        @SerializedName("address") val address: String,
        @SerializedName("productPrice") val productPrice: Double,
        @SerializedName("user") val user: UserSignUp,
        @SerializedName("deliveryPrice") val deliveryPrice: Double,
        @SerializedName("totalPrice") val totalPrice: Int,
        @SerializedName("notes") val notes: String,
        @SerializedName("market") val market: Shop,
        @SerializedName("creationDate") val creationDate: String
): Parcelable