package com.hamza.solutions.kolo.ui.order.products

import com.example.computec.eltadreb.ui.base.MvpPresenter
import com.example.computec.eltadreb.ui.base.MvpView
import com.hamza.solutions.kolo.model.CancleOrderResponse
import com.hamza.solutions.kolo.model.ProductsOrders

interface
OrderProductsContract {

    interface View : MvpView {
        fun onProductsOrdersRecived(productsOrders: List<ProductsOrders>)

    }

    interface Presenter<V : View> : MvpPresenter<V> {
        fun getProductsOrders(userId: String)

    }
}