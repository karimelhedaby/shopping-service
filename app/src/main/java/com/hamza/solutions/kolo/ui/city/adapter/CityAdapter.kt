package com.hamza.solutions.kolo.ui.city.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.City
import kotlinx.android.synthetic.main.city_row.view.*

class CityAdapter(var context: Context, var cities: List<City>, var citylistener: cityListener) : RecyclerView.Adapter<CityAdapter.CityVH>() {


    class CityVH(itemView: View) : RecyclerView.ViewHolder(itemView) {


    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityVH {
        return CityVH(LayoutInflater.from(parent.context)
                .inflate(R.layout.city_row, null))
    }

    override fun onBindViewHolder(holder: CityVH, position: Int) {
        with(holder.itemView) {

            Glide.with(context)
                    .load(cities.get(position).image)
                    .into(cityIV)

            cityTV.text = cities.get(position).name

            holder.itemView.setOnClickListener {
                citylistener.onCityClickListener(cities.get(position), position)
            }
        }
    }

    open interface cityListener {

        fun onCityClickListener(city: City, position: Int)
    }

    override fun getItemCount(): Int {
        return cities.size
    }
}


