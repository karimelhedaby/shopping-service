package com.hamza.solutions.kolo.ui.order.offer

import com.example.computec.eltadreb.ui.base.BasePresenter
import com.hamza.solutions.kolo.R
import com.hamza.solutions.kolo.model.Order
import com.hamza.solutions.kolo.model.OrderOffer
import com.rawaa.hamza.rawaa.service.AppDataManager
import com.rawaa.hamza.rawaaprovider.service.DataManager
import com.rawaa.hamza.rawaaprovider.service.retrofit.CallbackWrapper
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class OrderOfferPresenter<V : OrderOffersContract.View> : BasePresenter<V>(), OrderOffersContract.Presenter<V> {

    override fun getOrderOffers(id: String) {

        // Timber.d("test send")
        if (mvpView!!.isNetworkConnected()) {
            // Timber.d("test send")

            mvpView?.showLoading()
            dataManager.getOrderOffers(id)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeWith(object : CallbackWrapper<List<OrderOffer>>(mvpView!!) {

                        override fun onSuccess(t: List<OrderOffer>) {
                            // Timber.d("success send")
                            mvpView?.hideLoading()
                            mvpView?.onOrderOfferRecived(t)
                        }

                    }
                    )

        } else {
            mvpView?.onError(R.string.error_no_internet_connection)
        }
    }

    private var dataManager: DataManager = AppDataManager()
}