package com.hamza.solutions.kolo.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

data class Shop(
        @SerializedName("logo") val logo: String,
        @SerializedName("cover") val cover: String,
        @SerializedName("_id") val id: String,
        @SerializedName("nameOfShop") val nameOfShop: String,
        @SerializedName("categoryId") val categoryId: Any,
        @SerializedName("genderTarget") val genderTarget: String,
        @SerializedName("creationDate") val creationDate: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            Any(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeString(logo)
        writeString(cover)
        writeString(id)
        writeString(nameOfShop)
//        writeString("")
        writeString(genderTarget)
        writeString(creationDate)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<Shop> = object : Parcelable.Creator<Shop> {
            override fun createFromParcel(source: Parcel): Shop = Shop(source)
            override fun newArray(size: Int): Array<Shop?> = arrayOfNulls(size)
        }
    }
}